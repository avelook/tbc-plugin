jQuery( function ( )
{


//coords localisations
    jQuery( document ).on( "change", ".js-toolset-google-map.textfield", function ( ) {

	jQuery( "input[name='wpcf[localisation-latitude]'], input[name='wpcf[localisation-longitude]']" ).val( "Mise à jour des coordonnées en cours..." );
	setTimeout( function ( ) {

	    var latitude = jQuery( ".js-toolset-google-map-lat" ).val( );
	    var longitude = jQuery( ".js-toolset-google-map-lon" ).val( );
	    jQuery( "input[name='wpcf[localisation-latitude]']" ).val( latitude );
	    jQuery( "input[name='wpcf[localisation-longitude]']" ).val( longitude );
	}, 3000 );
    } );
    select_localisation.init( );
} );
/************************************* SEARCH ENGINE MAJ CITIES DEPENDING COUNTRY     ***********************************************************/


var select_localisation = {
    init: function ( )
    {

	jQuery( "select[name='wpcf[pays]']" ).on( "change", function ()
	{

	    selected = jQuery( this ).val( );
	    if ( selected != '' )
	    {
		select_localisation.majcities( selected, null );
	    }
	} );

	jQuery( "select[name='wpcf[ville]']" ).on( "change", function ()
	{
	    selected = jQuery( this ).val( );
	    if ( selected != '' )
	    {
		select_localisation.majArr( selected );
	    }

	} );
    },
    majcities: function ( country, city )
    {
	var data = { 'country': country, 'action': 'getcities' };
	var city = city;
	jQuery.ajax(
	    {
		type: "POST",
		context: this,
		url: tbc_vars.ajaxurl,
		data: data,
		cache: false,
		beforeSend: function ( )
		{
		    select_localisation.showPreloader( );
		},
		success: function ( msg )
		{
		    select_localisation.hidePreloader( );
		    console.log( msg );
		    jQuery( "select[name='wpcf[ville]']" ).html( msg );
		    select_localisation.selectCity( city );
		    //response = JSON.stringify( msg );
		}
	    } );
    },
    majArr: function ( city )
    {
	var data = { 'city': city, 'action': 'getarrs' };
	jQuery.ajax(
	    {
		type: "POST",
		context: this,
		url: tbc_vars.ajaxurl,
		data: data,
		cache: false,
		beforeSend: function ( )
		{
		    select_localisation.showPreloader( );
		},
		success: function ( msg )
		{
		    select_localisation.hidePreloader( );
		    jQuery( "select[name='wpcf[arrondissement]']" ).html( msg );
		    //    select_localisation.selectCity( city );
		    //response = JSON.stringify( msg );
		}
	    } );

    },
    selectCountry: function ( country, callbackfn )
    {
	jQuery( ".home #wpv_control_select_wpv-wpcf-pays option[value='" + country + "']" ).attr( "selected", "selected" );
	if ( callbackfn != null )
	{
	    callbackfn( );
	}

    },
    selectCity: function ( city )
    {
	jQuery( "select[name='wpv-wpcf-ville[]'] option[value=" + city + "]" ).attr( "selected", "selected" );
    },
    showPreloader: function ( )
    {
	var pending = "<span id='citiesloader'>loading...</span>";
    },
    hidePreloader: function ( )
    {
	jQuery( "#citiesloader" ).remove( );
    }


};

