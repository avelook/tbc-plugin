<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.avelook.fr
 * @since      1.0.0
 *
 * @package    Tbc
 * @subpackage Tbc/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Tbc
 * @subpackage Tbc/admin
 * @author     avelook <raphael.thiry@avelook.fr>
 */
class Tbc_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Tbc_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Tbc_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/tbc-admin.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Tbc_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Tbc_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/tbc-admin.js', array('jquery'), $this->version, false);
    }

    public function admin_footer()
    {
        global $wp_query;
        $entry_id = $wp_query->get('entry');
        echo '<script type="text/javascript">';
        echo '
        var tbc_vars=
        {
        base_url:"' . get_site_url() . '",
         ajaxurl: "' . get_site_url() . '/wp-content/plugins/tbc-plugin/admin/admin-ajax.php",
        securite_nonce: " ' . wp_create_nonce('securite-nonce') . '",
         puploads:"' . get_site_url() . '/wp-content/uploads/tbc/pdf/' . $entry_id . '/' . '",
         tbc_plugin_public:"' . plugin_dir_url('') . 'tbc-plugin/public",
        sending_proposal:
         {
         success_heading: "Envoi réussi",
         success_message:"<p>' . __('Votre proposition commerciale a bien été envoyée. Une copie a  été envoyée au service commercial de TBC', 'tbc_plugin') . '</p><p>' . __('La page va se recharger, merci de patienter...', 'tbc_plugin') . '</p>"
          }}
      </script>';
    }
    public function set_custom_edit_centre_columns($columns)
    {

        $columns['email-centre'] = __('Email Contact', 'tbc_admin');
        $columns['localisation'] = __('Localisation', 'tbc_admin');
        $columns['leads']        = __('Leads', 'tbc_admin');
        return $columns;
    }
    public function custom_centre_column($column, $post_id)
    {
        switch ($column) {

            case 'email-centre':
                echo "<span style='font-weight:bold'>" . get_post_meta($post_id, 'wpcf-email-centre', true) . "</span>";
                break;

            case 'localisation':
                $ville = get_term(get_post_meta($post_id, 'wpcf-ville', true), "localisation")->name;
                $pays  = get_term(get_post_meta($post_id, 'wpcf-pays', true), "localisation")->name;
                $arr   = " " . get_term(get_post_meta($post_id, 'wpcf-arrondissement', true), "localisation")->name;
                echo $ville . $arr . " (" . $pays . ")";
                break;

            case 'leads':
                $filter['mode']                   = 'any';
                $filter[]                         = array('key' => 23, 'operator' => 'contains', 'value' => '"' . $post_id . '"');
                $search_criteria['field_filters'] = $filter;
                $leads_all                        = GFAPI::get_entries(1, $search_criteria, $sorting);
                echo count($leads_all);
                break;
        }
    }

}
