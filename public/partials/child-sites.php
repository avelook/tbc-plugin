<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.avelook.fr
 * @since      1.0.0
 *
 * @package    Tbc
 * @subpackage Tbc/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Tbc
 * @subpackage Tbc/public
 * @author     avelook <raphael.thiry@avelook.fr>
 */
class Tbc_ChildSites
{

    public function getNewsWidget()
    {

        $url = 'https://www.team-business-centers.com/wp-json/wp/v2/posts?categories=14';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
//  curl_setopt($ch,CURLOPT_HEADER,TRUE);
        //  curl_setopt($ch,CURLOPT_NOBODY,TRUE); // remove body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content  = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result   = $ch;
        curl_close($ch);

        $posts   = json_decode($content);
        $content = "<section id='tbc_widget_news' class='text-center'>";
        $content .= "<div class='container'>";
        $content .= "<div id='carousel_news' class='owl-carousel'>";
        foreach ($posts as $post) {
            $content .= "<div class='item'><h3><a target='blank' href='" . $post->link . "'>" . $post->title->rendered . "</a></h3><p>" . wp_trim_words(do_shortcode($post->content->rendered), 20, '...') . "</p></div>";
        }
        $content .= "</div>";
        $content .= "</div>";
        $content .= "</section>";
        $jsinline = "jQuery(function()
                            {

                            jQuery('#carousel_news').owlCarousel({
                                items:3,
                                loop:true,
                                margin:10
                            });
                            })
                            ";

        return $content;
    }

}
