<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tbc_Helper
{

//get centers
    public static function getCenters()
    {

        $args = array('post_type' => 'centre',
            'post_status'             => 'publish',
            'posts_per_page'          => -1,
            'orderby'                 => 'post_title',
            'order'                   => 'ASC');
        $posts = get_posts($args);

        return $posts;
    }
    /*
     * get centers of a user
     */
    public static function getUserCenters($user_id)
    {
        $key          = "centers";
        $single       = true;
        $user_centers = get_user_meta($user_id, $key, $single);
        if ($user_centers == "") {
            return false;
        } else {
            $user_centers = explode(",", $user_centers);
        }
        return $user_centers;
    }

    /*
     * Get all emails associated to an Entry
     * Take all centers + email center + user email connected to each center
     */
    public static function getCentersEmailsEntry($entry)
    {

        $centers = json_decode($entry[23]);
        $emails  = [];
        foreach ($centers as $center) {
            $emails[] = get_post_meta($center, "wpcf-email-centre", true);
            $emails   = array_merge(self::getCenterEmailsUsers($center), $emails);
        }
        $emails = array_unique($emails); //remove duplicated

        return $emails;

    }

//get email users of a given center
    public static function getCenterEmailsUsers($center)
    {
        $users = [];

        $args = array(
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key'     => 'centers',
                    'value'   => '' . $center . ',',
                    'compare' => 'LIKE',
                ), array(
                    'key'     => 'centers',
                    'value'   => ',' . $center . ',',
                    'compare' => 'LIKE',
                ), array(
                    'key'     => 'centers',
                    'value'   => ',' . $center . '',
                    'compare' => 'LIKE',
                ),
                array(
                    'key'     => 'centers',
                    'value'   => '' . $center . '',
                    'compare' => 'IS',
                ),
            ),
        );
        $user_query = new WP_User_Query($args);
        $results    = $user_query->get_results();

        $emails = [];
        foreach ($results as $result) {
            $emails[] = $result->user_email;
        }

        return $emails;
    }

//get email users of a given center
    public static function getUsersByCenters($centers)
    {
        $users = [];

        foreach ((array) $centers as $center) {

            $args = array(
                'meta_query' => array(
                    'relation' => 'OR',
                    array(
                        'key'     => 'centers',
                        'value'   => '' . $center . ',',
                        'compare' => 'LIKE',
                    ), array(
                        'key'     => 'centers',
                        'value'   => ',' . $center . ',',
                        'compare' => 'LIKE',
                    ), array(
                        'key'     => 'centers',
                        'value'   => ',' . $center . '',
                        'compare' => 'LIKE',
                    ),
                    array(
                        'key'     => 'centers',
                        'value'   => '' . $center . '',
                        'compare' => 'IS',
                    ),
                ),
            );
            $user_query = new WP_User_Query($args);
            $results    = $user_query->get_results();
            $users      = array_merge($users, $results);
        }

        $recipients = [];
        foreach ($users as $user) {
            $recipients[] = $user->user_email;
        }

        return $recipients;
    }
//get the current center of a given page
    public static function getCurrentCenter()
    {

        //center post
        $post = get_post();
        $type = get_post_type();

        switch ($type) {
            case "centre":
                $current_center = $post->ID;
                break;

            case "bureau":
            case "salle-de-reunion":
            case "domiciliation":
                $current_center = get_post_meta($post->ID, "_wpcf_belongs_centre_id", true);
                break;
        }

        return $current_center;
    }

    /*
     * Get country from localisation taxonomy
     */
    public static function getCountries()
    {
        $countries = get_terms(
            array(
                'taxonomy'   => 'localisation',
                'parent'     => 0,
                'hide_empty' => false,
            ));
        return $countries;
    }

    public static function getCities()
    {

        $countries = self::getCountries();

        $cities = array();
        foreach ($countries as $country) {
            $args = array(
                'taxonomy'   => 'localisation',
                'parent'     => $country->term_id,
                'hide_empty' => false,
            );
            $cities = array_merge($cities, get_terms($args));
        }

        return $cities;
    }

    /*
     * count workspaces
     */
    public static function getWorkSpaces()
    {

        $centers = self::getCenters();
        foreach ($centers as $center) {
            $results[]['bureaux']   = get_post_meta($center->ID, 'wpcf-nombre-de-bureaux');
            $results[]['sdr']       = get_post_meta($center->ID, 'wpcf-nombre-de-salles-de-reunion');
            $results[]['coworking'] = get_post_meta($center->ID, 'wpcf-nombre-de-postes-en-coworking');
        }

        return $results;
    }

}
