<?php

class Tbc_Dashboard
{

    private function getFilters($type)
    {

        switch ($type) {
            case "date":

                return [
                    ['value' => 'today', 'title' => 'aujourd\'hui'],
                    ['value' => 'thismonth', 'title' => 'ce mois-ci'],
                    ['value' => 'last3months', 'title' => '3 derniers mois'],
                    ['value' => 'thisyear', 'title' => 'cette année', 'selected' => 'selected'],
                ];

                break;

            case "centers":

                $centers = Tbc_Helper::getCenters();
                $return  = [];
                foreach ($centers as $center) {
                    $return[] = ['value' => $center->ID, 'title' => $center->post_title];
                }
                return $return;
                break;

            case "origin":
                $form    = 1;
                $origins = GFFormsModel::get_field($form, 15)->choices;
                foreach ($origins as $item) {
                    $return[] = ['value' => $item['value'], 'title' => $item['text']];
                }
                return $return;
                break;
        }
    }
    /*
     * Render filters Form
     */
    public function getFiltersForm()
    {
        ob_start();
        ?>
            <form id="dashboardForm">
            <select id="dashbselect-types" name="date" class="custom-select">
                    <?php
foreach ($this->getFilters('date') as $date) {
            echo '<option ' . $date['selected'] . ' value="' . $date['value'] . '">' . $date['title'] . '</option>';
        }
        ?>
            </select>

            <select id="dashbselect-centers" name="center" class="custom-select">
                    <?php
echo '<option value="" selected>Tous les centres</option>';
        foreach ($this->getFilters('centers') as $center) {
            echo '<option value="' . $center['value'] . '">' . $center['title'] . '</option>';
        }
        ?>
            </select>

                <select id="dashbselect-origin" name="origin" class="custom-select">
                    <?php
echo '<option value="" selected>Toutes les origines</option>';
        foreach ($this->getFilters('origin') as $origin) {
            echo '<option value="' . $origin['value'] . '">' . $origin['title'] . '</option>';
        }
        ?>
                </select>
                </form>
                <?php
$content = ob_get_contents();
        ob_clean();
        return $content;
    }
    /*
     * Get type repartition widget
     */
    public function getTypeWidget()
    {
        ob_start();
        ?>

        <div class="row">

                <div class="col-lg-12 col-md-12">
                <div class="card">
                            <?php echo $this->getFiltersForm(); ?>


                </div>
                    </div>
            </div>

                <div class="row">
                            <div class="col-lg-4 col-md-4">
                        <div class="card">
                        <div class="card-block">
                        <h3 class="card-title">Répartition des demandes par types </h3>
                        <h6 class="card-subtitle">Types de demandes</h6>
                        <div class="ct-chart-wrapper">
                            <div class="ct-chart ct-chart-types" style="height:290px; width:100%;"></div>
                            <div class="ct-chart-tabletypes"></div>
                        </div>

                        </div>
                        <div>
                        <hr class="m-t-0 m-b-0">
                        </div>
                                <div class="card-block text-center " style="display:none">
                            <ul class="list-inline m-b-0">
                            <li>
                            <h6 class="text-muted text-info"><i class="fa fa-circle font-10 m-r-10 "></i>Mobile</h6> </li>
                            <li>
                            <h6 class="text-muted  text-primary"><i class="fa fa-circle font-10 m-r-10"></i>Desktop</h6> </li>
                            <li>
                            <h6 class="text-muted  text-success"><i class="fa fa-circle font-10 m-r-10"></i>Tablet</h6> </li>
                        </ul>
                        </div>
                    </div>
                            </div>

                            <div class="col-lg-4 col-md-">
                        <div class="card">
                            <div class="card-block">
                            <h3 class="card-title">Répartition  des demandes par statut </h3>
                            <h6 class="card-subtitle">Statuts des demandes de contact</h6>
                                    <div class="ct-chart-wrapper">
                                <div class="ct-chart ct-chart-states" style="height:290px; width:100%;"></div>
</div>

                            </div>
                            <div>
                            <hr class="m-t-0 m-b-0">
                            </div>
                                    <div class="card-block text-center " style="display:none">
                                <ul class="list-inline m-b-0">
                                <li>
                                <h6 class="text-muted text-info"><i class="fa fa-circle font-10 m-r-10 "></i>Mobile</h6> </li>
                                <li>
                                <h6 class="text-muted  text-primary"><i class="fa fa-circle font-10 m-r-10"></i>Desktop</h6> </li>
                                <li>
                                <h6 class="text-muted  text-success"><i class="fa fa-circle font-10 m-r-10"></i>Tablet</h6> </li>
                            </ul>
                            </div>
                        </div>
                                    </div>




                    </div>


        <div class="row" style="display: none">
        <div class="col-lg-4 col-xlg-3 col-md-4 col-sm-12 b-r">
            <div class="card-body">
                <div class="d-flex no-block align-self-center">
                <div>
                            <h3>Répartition des demandes par type</h3>
                        <h6 class="card-subtitle">Demandes de contact</h6>
                    </div>
                <div class="ml-auto">

                    </div>
                </div>
                <div class="row">
                <div class="col-lg-12 m-t-40">
                    <h1 class="m-b-0 font-light">31568</h1>
                    <h6 class="text-muted">Page views</h6></div>
                <div class="col-lg-12 m-t-40">
                    <h1 class="m-b-0 font-light">8457</h1>
                    <h6 class="text-muted">New Visits</h6></div>
                </div>
            </div>
            </div>
            <div class="col-lg-8 col-xlg-9 col-md-8 col-sm-12 align-self-center">
                    <div class="card-body">
                    <div class="d-flex no-block align-self-center">
                            <h3 class="card-title">Types de demandes</h3>

                                <div class="ml-auto">

                                <select style="display:none" class=" custom-select">
                            <option selected="">January</option>
                        <option value="1">February</option>
                        <option value="2">March</option>
                        <option value="3">April</option>
                        </select>
                    </div>
                    </div>
                    <div id="m-piechart" style="width: 100%; height: 278px; -moz-user-select: none; background-color: rgba(0, 0, 0, 0); cursor: default;" _echarts_instance_="1543486175422"><div style="position: relative; overflow: hidden; width: 608px; height: 278px;"><div style="position: absolute; left: 0px; top: 0px; width: 608px; height: 278px; -moz-user-select: none;" data-zr-dom-id="bg" class="zr-element"></div><canvas style="position: absolute; left: 0px; top: 0px; width: 608px; height: 278px; -moz-user-select: none;" width="608" height="278" data-zr-dom-id="0" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 608px; height: 278px; -moz-user-select: none;" width="608" height="278" data-zr-dom-id="1" class="zr-element"></canvas><canvas style="position: absolute; left: 0px; top: 0px; width: 608px; height: 278px; -moz-user-select: none;" width="608" height="278" data-zr-dom-id="_zrender_hover_" class="zr-element"></canvas><div class="echarts-tooltip zr-element" style="position: absolute; display: none; border-style: solid; white-space: nowrap; transition: left 0.4s ease 0s, top 0.4s ease 0s; background-color: rgba(0, 0, 0, 0.7); border-width: 0px; border-color: rgb(51, 51, 51); border-radius: 4px; color: rgb(255, 255, 255); padding: 5px; left: 369px; top: 0px;">Source <br>80% : 335 (34.22%)</div></div></div>
                    <center>
                    <ul class="list-inline m-t-20">
                        <li>
                        <h6 class="text-muted"><i class="fa fa-circle m-r-5 text-success"></i>Mobile</h6> </li>
                        <li>
                        <h6 class="text-muted"><i class="fa fa-circle m-r-5 text-primary"></i>Desktop</h6> </li>
                        <li>
                        <h6 class="text-muted"><i class="fa fa-circle m-r-5 text-danger"></i>Tablet</h6> </li>
                        <li>
                        <h6 class="text-muted"><i class="fa fa-circle m-r-5 text-muted"></i>Other</h6> </li>
                    </ul>
                    </center>
                </div>
                </div>
        </div>


    <?php
$content = ob_get_contents();
        ob_clean();
        return $content;
    }
    /*
     * Render Dashboard Admin
     */
    public function getDashboardAdmin()
    {

        echo $this->getTypeWidget();
    }
    /* QUERY ********************************************************************************* */

    /*
     * Get request Date criteria
     *
     */
    public function getDateCriteria($period)
    {
        switch ($period) {
            case 'today':

                //LAST 3 MONTHS
                $fd              = new DateTime("now");
                $start_date      = $fd->format("Y-m-d");
                $ld              = new DateTime("tomorrow");
                $end_date        = $ld->format("Y-m-d");
                $search_criteria = array(
                    'start_date' => $start_date,
                    'end_date'   => $end_date,
                );
                break;

            case 'thismonth':

                //MONTHLY
                $fd              = new DateTime("first day of this month");
                $start_date      = $fd->format("Y-m-d");
                $ld              = new DateTime("now");
                $end_date        = $end_date        = $ld->format("Y-m-d");
                $search_criteria = array(
                    'start_date' => $start_date,
                    'end_date'   => $end_date,
                );

                break;

            case 'lastmonth':
                //LAST MONTH
                $fd              = new DateTime("first day of previous month");
                $start_date      = $fd->format("Y-m-d");
                $ld              = new DateTime("last day of previous month");
                $end_date        = $ld->format("Y-m-d");
                $search_criteria = array(
                    'start_date' => $start_date,
                    'end_date'   => $end_date,
                );

                break;

            case 'last3months':

                //LAST 3 MONTHS
                $fd              = new DateTime("first day of - 3 months");
                $start_date      = $fd->format("Y-m-d");
                $ld              = new DateTime("now");
                $end_date        = $ld->format("Y-m-d");
                $search_criteria = array(
                    'start_date' => $start_date,
                    'end_date'   => $end_date,
                );
                break;

            case 'thisyear':
                //CURRENT YEAR
                $fd              = new DateTime('first day of January ' . date('Y'));
                $start_date      = $end_date      = $fd->format("Y-m-d");
                $now             = new DateTime("now");
                $end_date        = $end_date        = $now->format("Y-m-d");
                $search_criteria = array(
                    'start_date' => $start_date,
                    'end_date'   => $end_date,
                );
                break;
        }

        return $search_criteria;
    }

    /*
     * Get entries after apply criteria filters
     */
    public function getEntries($values)
    {
        foreach ($values as $row) {
            if ($row['value'] == '') {
                continue;
            }

            switch ($row['name']) {
                case "date":
                    $search_criteria = $this->getDateCriteria($row['value']);
                    break;

                case "center":

                    //    $filter['mode']    ='any';
                    $filter[] = array('key' => 23, 'operator' => 'contains', 'value' => $row['value']);

                    /*    $search_criteria['field_filters']=[ 'key'        =>'13',
                    'operator'    =>'in',
                    'value'        =>$row['value']
                    ]; */

                    break;

                case "origin":

                    //    $filter['mode']    ='any';
                    $filter[] = array('key' => 15, 'value' => $row['value']);
                    break;
            }

            $search_criteria['field_filters'] = $filter;
        }

        //var_dump($search_criteria);

        $sorting = array('created' => 'DESC');
        $paging  = array('offset' => 0, 'page_size' => 9999999); //important
        $form    = 1;
        $entries = GFAPI::get_entries($form, $search_criteria, $sorting, $paging); //get the last lead -1 because hook triggered after form submission
        /* var_dump($search_criteria);
        var_dump($entries); */

        return $entries;
    }
    /*
     * Get type Stats widget
     */
    public function getStats()
    {
        if (wp_verify_nonce($_GET['securite_nonce'], 'securite-nonce')) {
            $values = $_GET['values'];

            $entries = $this->getEntries($values);
            $form_id = 1;
            $form    = RGFormsModel::get_form_meta($form_id);

            // STATES
            $states = GFFormsModel::get_field($form, 13)->choices;
            $state  = [];
            foreach ($states as $st) {
                $state[$st['value']][0] = $st['text'];
                $state[$st['value']][1] = 0;
            }

            //TYPES
            $types = GFFormsModel::get_field($form, 11)->choices;
            $t     = [];
            //limit to this list
            $types_list = ['bureau', 'salles-de-reunion', 'coworking', 'accueil-telephonique', 'domiciliation-entreprise'];

            foreach ($types as $st) {
                if (in_array($st['value'], $types_list)) {
                    $t[$st['value']][0] = $st['text'];
                    $t[$st['value']][1] = 0;
                }
            }

            $types_items = 0;

            // LOOP IN ENTRIES
            foreach ($entries as $entry) {

                //states
                $state[$entry[13]][1]++;

                //types
                $field       = GFFormsModel::get_field($form, 11);
                $field_value = is_object($field) ? $field->get_value_export($entry) : '';
                $varray      = str_replace(" ", "", $field_value);
                $types       = explode(",", $varray);

                foreach ($types as $type) {
                    if (in_array($type, $types_list)) {
                        $t[$type][1]++;
                        $types_items++;
                    }
                }
            }

            $return = [];

            //prepare output
            foreach ($state as $out) {
                $total_entries      = count($entries);
                $return['states'][] = [$out[0], (($out[1] / $total_entries) * 100)];
            }
            foreach ($t as $out) {
                $return['types'][] = [$out[0], (($out[1] / $types_items) * 100)];
            }

            wp_send_json($return);
        }
    }

}
