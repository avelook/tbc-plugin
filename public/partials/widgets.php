<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Tbc_Widgets
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version )
    {

	$this->plugin_name	=$plugin_name;
	$this->version		=$version;
    }
    /*
     * Prepare Stat Widget
     */
    public function getStatsTbcWidget()
    {
	$adresses	=count(Tbc_Helper::getCenters());
	$villes		=count(Tbc_Helper::getCities());
	$espaces	=count(Tbc_Helper::getWorkSpaces());
	$clients	=$adresses*150;



	$values=[array ('text'=>__('adresses','tbc_plugin'),'value'=>$adresses),
	    array('text'=>__('villes couvertes','tbc_plugin'),'value'=>$villes),
	array('text'=>__('espaces de travail','tbc_plugin'),'value'=>$espaces),
	array ('text'=>__('clients','clients'),'value'=>$clients)
	];

	$content="<section id='tbc_widget_stats' class='text-center text-white  d-flex flex-row align-items-center'>";
	$content.="<div class='container'>";
	$content.="<div  class='row'>";

	
	foreach ($values as $key=> $item)
	{
	    $content.="<div class='col-md-3 col-sm-6 col-6 stat_box statitem".$key."'><p class='stat_value'>0</p><p class='stat_text'>".$item['text']."</p></div>";
	}
	$content.="</div>";
	$content.="</div>";
	$content.="</section>";


	$content.='<script>jQuery(function() {
	    jQuery(".stat_box").viewportChecker({
		   callbackFunction: function ()
		   {      ';

	$content.='        jQuery(".statitem0 .stat_value").countTo({
	from: 0,
                to: '.$values[0]['value'].',
                speed: 2000,
                refreshInterval: 500,
                onComplete: function (value) {
                }
	     });';
	$content.='        jQuery(".statitem1 .stat_value").countTo({
	from: 0,
                to: '.$values[1]['value'].',
                speed: 2000,
                refreshInterval: 500,
                onComplete: function (value) {
                }
	     });';
	$content.='        jQuery(".statitem2 .stat_value").countTo({
	from: 0,
                to: '.$values[2]['value'].',
                speed: 2000,
                refreshInterval: 100,
                onComplete: function (value) {
                }
	     });';
	$content.='        jQuery(".statitem3 .stat_value").countTo({
	from: 0,
                to: '.$values[3]['value'].',
                speed: 1500,
                refreshInterval: 50,
                onComplete: function (value) {
                }
	     });';

	$content.='
        }
    });';

	$content.='});</script>';



	return $content;
    }

      /*
     * Prepare News Widget Home
     */
    public function getNewsWidget()
    {

	$args	=array ('category'=>14,'published'=>1);
	$posts	=get_posts($args);

	$content="<section id='tbc_widget_news' class='text-center'>";
	$content.="<div class='container'>";
	$content.="<div id='carousel_news' class='owl-carousel'>";
	foreach ($posts as $post)
	{
	    $content.="<div class='item'><h3><a href='".get_permalink($post->ID)."'>".$post->post_title."</a></h3><p>".wp_trim_words(do_shortcode($post->post_content),20,'...')."</p></div>";
	}
	$content.="</div>";
	$content.="</div>";
	$content.="</section>";

	return $content;
    }
    /*
     * Prepare Stat Entries
     */
    public function getStatsEntriesWidget()
    {
	

	//TOTAL
	$result['total']=[
	    'title'	=>__('Total','tbc_plugin'),
	    'values'=>$this->_countStats(array ())
	];


	//MONTHLY
	$fd		=new DateTime("first day of this month");
	$start_date		=$fd->format("Y-m-d");
	$ld			=new DateTime("now");
	$end_date		=$end_date		=$ld->format("Y-m-d");
	$search_criteria=array (
	    'start_date'	=>$start_date,
	    'end_date'	=>$end_date
	);
	$result['monthly']	=[
	    'title'	=>__('Ce mois-ci','tbc_plugin'),
	    'values'=>$this->_countStats($search_criteria)
	];

	

	//LAST MONTH
	$fd			=new DateTime("first day of previous month");
	$start_date		=$fd->format("Y-m-d");
	$ld			=new DateTime("last day of previous month");
	$end_date		=$ld->format("Y-m-d");
	$search_criteria	=array (
	    'start_date'	=>$start_date,
	    'end_date'	=>$end_date
	);
	$result['last_month']	=[
	    'title'	=>__('Dernier mois','tbc_plugin'),
	    'values'=>$this->_countStats($search_criteria)
	];
	//LAST 3 MONTHS
	$fd			=new DateTime("first day of - 3 months");
	$start_date		=$fd->format("Y-m-d");
	$ld			=new DateTime("now");
	$end_date		=$ld->format("Y-m-d");
	$search_criteria	=array (
	    'start_date'	=>$start_date,
	    'end_date'	=>$end_date
	);
	$result['last_3_months']=[
	    'title'	=>__('3 derniers mois','tbc_plugin'),
	    'values'=>$this->_countStats($search_criteria)
	];

	//CURRENT YEAR
	$fd			=new DateTime('first day of January '.date('Y'));
	$start_date		=$end_date		=$fd->format("Y-m-d");
	$now			=new DateTime("now");
	$end_date		=$end_date		=$now->format("Y-m-d");
	$search_criteria	=array (
	    'start_date'	=>$start_date,
	    'end_date'	=>$end_date
	);
	$result['current_year']	=[
	    'title'	=>__('Année en cours','tbc_plugin'),
	    'values'=>$this->_countStats($search_criteria)
	];



	$content="<div class='row clearfix'>";

	$content.="<div id='members_stats' class='col-md-12'>";

	$content.="<ul class='nav nav-pills'>";
	$i=0;
	foreach ($result as $res)
	{
	
	  
	    if ($i==0):$class="active show";
	    else: $class="";
	    endif;
	
	    $content.='<li class=" '.$class.'">
		      <a  href="#v'.$i.'" data-toggle="tab">'.$res['title'].'</a>
			
			</li>';
	    $i++;
	}
	 $content.="</ul>";

	$content.='<div class="tab-content clearfix">';


	$i=0;
	foreach ($result as $res)
	{
	
	    if ($i==0):$class="active";
	    else: $class="";
	    endif;
	    $content.='<div class="tab-pane row  '.$class.'" id="v'.$i.'">';
	  //  $content.="<h4>".$res['title']."</h4>";




	    $bg	=['bg-aqua','bg-yellow','bg-green','bg-red'];
	    $icon	=['fa-bell-o','fa-clock-o','fa-thumbs-o-up',' fa-thumbs-o-down'];

	    foreach ($res['values'] as $key=> $stat)
	    {
		
		$content.="<div  class='col-md-3 small-box'><div class='wrapper  ".$bg[$key]."'><div class='icon'> <i class='fa ".$icon[$key]."'></i></div><p class='statval'>".$stat['value']."</p><p class='stattext'>".$stat['text']."</p></div></div>";
	    }
	
	    $content.="</div>";
	    $i++;
	}
	$content.="</ul>";
	$content.="</div>";


	$content.="</div>";
	$content.="</div>";


	return $content;
    }




    public function getBannerResultsWidget()
    {

	$array=['taxonomy'=>'localisation'];

	if ($_GET["wpv-wpcf-ville"][0]!='')
	{
	    $location_id=$_GET["wpv-wpcf-ville"][0];
	}
	else
	     {
	    $location_id=$_GET["wpv-wpcf-pays"][0];
	}

	$location	=get_term_by('id',$location_id,'localisation');
	$photo		=get_term_meta($location_id,"wpcf-photo")[0];
	$desc		=get_term_meta($location_id,"wpcf-header-page-resultats")[0];


	if ($photo==null)
	{
	    $photo=get_site_url()."/wp-content/uploads/2018/05/BELGIQUE_1800x400.png";
	}

	$content='';
	$content.='<div id="results_banner" class="banner d-flex align-items-center" style="background:url(\''.$photo.'\')">';
	$content.='<div class="overlay"></div>';
	$content.='<div class="container text-center">'.do_shortcode($desc).'<p style="margin-top:8px"><a class="btn btn-fushia btn-wp page-scroller" href="#sidebar_right">'.__('Contactez-nous','tbc_plugin').'</a></p></div></div>';



	return $content;
    }
    public function getSearchHomeWidget()
    {

	$content='	<!----START HOME BANNER--->
	        <div id="search_home">
		<ul id="tabproducts" class="nav">
    		    <li class="col-md-3 col-sm-6 col-6 "> <a class="active btn-bureaux" data-toggle="pill"  href="#home" bg-url="'.get_template_directory_uri().'/assets/images/bg-bureaux.jpg">
			<img width="42"  src="'.get_template_directory_uri().'/assets/images/pic-bureau.png" /><span class="text">
		  '.__('Bureaux équipés','tbc_plugin').'</span></a></li>
    		    <li  class="col-md-3 col-sm-6 col-6 "><a data-toggle="pill" class=" btn-sdr" href="#menu1" bg-url="'.get_template_directory_uri().'/assets/images/bg-sdr.jpg">
			<img width="42" src="'.get_template_directory_uri().'/assets/images/pic-sdr.png" /><span class="text">'.__('Salles de réunion','tbc_plugin').'</span></a></li>
    		    <li  class="col-md-3 col-sm-6 col-6 "><a class=" btn-coworking" data-toggle="pill" href="#menu2" bg-url="'.get_template_directory_uri().'/assets/images/bg-coworking.jpg">
			<img width="42" src="'.get_template_directory_uri().'/assets/images/pic-coworking.png" /><span class="text">'.__('Coworking','tbc_plugin').'</span></a></li>
    		    <li  class="col-md-3 col-sm-6 col-6 "><a data-toggle="pill" class=" btn-dom" href="#menu3" bg-url="'.get_template_directory_uri().'/assets/images/bg-dom.jpg">
			<img width="22" src="'.get_template_directory_uri().'/assets/images/pic-dom.png" /><span class="text">'.__('Domiciliation','tbc_plugin').'</span></a></li>
    		</ul>

    		<div class="tab-content">
    		    <div id="home" class="tab-pane  in active show">
			    '.do_shortcode(' [wpv-form-view name="recherche-bureau" target_id="46"]').'
    		    </div>
    		    <div id="menu1" class="tab-pane">
			'.do_shortcode(' [wpv-form-view name="recherche-salle-de-reunion" target_id="137"]').'
						</div>
    		    <div id="menu2" class="tab-pane">
		'.do_shortcode(' [wpv-form-view name="recherche-coworking" target_id="145"]').'
						</div>
    		    <div id="menu3" class="tab-pane">
		'.do_shortcode(' [wpv-form-view name="recherche-de-domiciliation" target_id="147"]').'
						</div>
    		</div>
		</div>
		<!----END HOME BANNER--->';

	return $content;
    }
    public function getSearchInnerWidget()
    {

	$target_ids=[
	    'seb'	=>127,
	    'sesdr'	=>137,
	    'seec'	=>145,
	    'sedom'	=>147];




	$content='<!---SEARCH ENGINE--->
 <div class="inner_sengine row no-gutters">
      <div id="prod_selector" class="col-md-3">
        <div id="prod_wrapper" class="sesdr">
 <select autocomplete="off" class="form-control">
   <option toggle="seb">Bureaux équipés</option>
    <option selected  toggle="sesdr">Salles de réunion</option>
    <option toggle="seec">Espaces de coworking</option>
    <option toggle="sedom">Domiciliation</option>
   </select>
           </div>
          </div>
   <div id="form_wrapper" class="col-md-9">
     <div id="seb" class="d-none">
'.do_shortcode('[wpv-form-view name="recherche-bureau" target_id="'.$target_ids['seb'].'"]').'
     </div>
             <div id="sesdr" class="">
'.do_shortcode('[wpv-form-view name="recherche-salle-de-reunion" target_id="'.$target_ids['sesdr'].'"]').'
              </div>
         <div id="seec" class="d-none">
'.do_shortcode('[wpv-form-view name="recherche-coworking" target_id="'.$target_ids['seec'].'"]').'
                    </div>
         <div id="sedom" class="d-none">
'.do_shortcode('[wpv-form-view name="recherche-de-domiciliation" target_id="'.$target_ids['sedom'].'"]').'
                  </div>

   </div>
</div>
 <!---END SEARCH ENGINE--->';
	return $content;
    }
    /*
     *
     * functions
     */
    public function _countStats( $search_criteria )
    {
	$current_user	=wp_get_current_user();
	$user_id	=$current_user->ID;
	$user_centers	=Tbc_Helper::getUserCenters($user_id);
	

	$search_criteria=array_merge($search_criteria,['status'=>'active']);

	//user centers limit
	if (in_array('subscriber',(array)$current_user->roles))
	{
	    $filter['mode']='any';
	    foreach ($user_centers as $center)
	    {
		    $filter[]=array ('key'=>23,'operator'=>'contains','value'=>'"'.$center.'"');
	    }

	    $search_criteria['field_filters']=$filter;
	}




	$paging		=array ('offset'=>0,'page_size'=>9999999);//important
	$leads_all	=GFAPI::get_entries(1,$search_criteria,$sorting,$paging,$null);

	//filter on "is approved" field. Override limitation of gravity (condition added here because impossible to include in search criteria logic)
	if (in_array('subscriber',(array)$current_user->roles))
	{$output=[];
	    foreach ($leads_all as $lead)
	    {
		if ($lead['is_approved']==1)
		{
		    $output[]=$lead;
		}
	 }
	    $leads_all=$output;
	}

	$total		=$current	=$win		=$loose		=0;
	foreach ($leads_all as $res)
	{
	    $total++;
	    if (in_array($res[13],array (1,2)))
	    {
		$current++;
	    }
	    if (in_array($res[13],array (4,5)))
	    {
		$win++;
	    }
	    if ($res[13]==3)
	    {
		$loose++;
	    }
	}

	return [
	    [
		'text'	=>__('total','tbc_plugin'),
		'value'	=>$total
	    ],
	    [
		'text'	=>__('en cours','tbc_plugin'),
		'value'	=>$current
	    ],
	    [
		'text'	=>__('gagnés','tbc_plugin'),
		'value'	=>$win
	    ],
	    [
		'text'	=>__('perdus','tbc_plugin'),
		'value'	=>$loose
	    ]
	];
    }

}
