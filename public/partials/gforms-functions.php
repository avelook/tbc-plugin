<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.avelook.fr
 * @since      1.0.0
 *
 * @package    Tbc
 * @subpackage Tbc/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Tbc
 * @subpackage Tbc/public
 * @author     avelook <raphael.thiry@avelook.fr>
 */
class Tbc_GForms
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version )
    {

	$this->plugin_name	=$plugin_name;
	$this->version		=$version;
    }
    /*
     * PREPOPULATE FORM FIELDS
     */
    public function populate_posts( $form )
    {



	foreach ($form['fields'] as &$field)
	{


	    switch ($field->inputName)
	    {

		case "centerlist" :

		    $choices=[];
		    $inputs	=[];
		    //Get centers
		    $posts	=TBC_helper::getCenters();


		    //get current center
		    $current_center=Tbc_Helper::getCurrentCenter();


		    // $input_id	=1;
		    $field_id=$field->id;



		    foreach ($posts as $post)
		    {

			$fr_id=icl_object_id($post->ID,'centre',false,'fr');
			if ($fr_id==icl_object_id($current_center,'centre',false,'fr'))
			{
			    $choices[]=array ('text'=>$post->post_title,'value'=>$fr_id,'isSelected'=>'selected');
			}
			else
			{
			    $choices[]=array ('text'=>$post->post_title,'value'=>$fr_id);
			}

			/*   $inputs[]=array ('label'=>$post->post_title,'id'=>"{$field_id}.{$input_id}");
			  $input_id++; */
		    }
		    $field->placeholder	=__('Sélectionnez un centre','tbc_plugin');
		    $field->choices		=$choices;


		    break;


		case "products_list":

		    $choices=[];
		    //  $inputs	=[];

		    $types=[
			'type-de-bureau'		=>[
			    'text'	=>__('Bureaux','tbc_plugin'),
			    'value'	=>'bureau'
			],
			'nombre-de-personnes'		=>[
			    'text'	=>__('Salles de réunion','tbc_plugin'),
			    'value'	=>'salles-de-reunion'
			],
			'nombre-de-postes-coworking'	=>[
			    'text'	=>__('Coworking','tbc_plugin'),
			    'value'	=>'coworking'
			]
		    ];


		    $input_id	=1;
		    $field_id	=$field->id;


		    foreach ($types as $k=> $type)
		    {
			$fields	=get_option('wpcf-fields');
			$options=$fields[$k]['data']['options'];

			if (isset($_GET['wpv-wpcf-'.$k]))
			{
			    $req_value=$_GET['wpv-wpcf-'.$k];

			    $choices[]=array ('text'=>$type['text'],'value'=>$type['value'],'isSelected'=>'isSelected');
			}
			else
			{
			    $choices[]=array ('text'=>$type['text'],'value'=>$type['value']);
			}
			$inputs[]=array ('label'=>$type['text'],'id'=>"{$field_id}.{$input_id}",'name'=>$type['value']);
			$input_id++;
			if ($input_id%10==0)
			{
			    $input_id++;
			}
			//subitems



			foreach ($options as $key=> $option)
			{


			    if ($key!='default')
			    {

				if ($option['value']==$req_value)
				{
				    $choices[]=array ('text'=>__($option['title'],'tbc_plugin'),'value'=>$type['value'].'-'.$option['value'],'isSelected'=>'isSelected');
				}
				else
				{
				    $choices[]=array ('text'=>__($option['title'],'tbc_plugin'),'value'=>$type['value'].'-'.$option['value']);
				}
				$inputs[]=array ('label'=>__($option['title'],'tbc_plugin'),'id'=>"{$field_id}.{$input_id}",'name'=>__($option['title']));

				$input_id++;

				if ($input_id%10==0)
				{
				    $input_id++;
				}
			    }
			}
		    }

		    //others
		    $others=[
			'accueil-telephonique'		=>[
			    'text'	=>__('Accueil téléphonique','tbc_plugin'),
			    'value'	=>'accueil-telephonique'
			],'domiciliation-entreprise'	=>[
			    'text'	=>__('Domiciliation d\'entreprise','tbc_plugin'),
			    'value'	=>'domiciliation-entreprise'
			],
			'carte-avantage-tbc'		=>[
			    'text'	=>__('Carte TBC','tbc_plugin'),
			    'value'	=>'carte-avantage'
			]
		    ];

		    foreach ($others as $key=> $other)
		    {
			$choices[]	=array ('text'=>$other['text'],'value'=>$other['value']);
			$inputs[]	=array ('label'=>$other['text'],'id'=>"{$field_id}.{$input_id}",'name'=>$other['text']);
			$input_id++;
			if ($input_id%10==0)
			{
			    $input_id++;
			}
		    }

		    $field->choices	=$choices;
		    $field->inputs	=$inputs;

		    break;
	    }
	}




	return $form;
    }
    /*
     * AFTER SUBMISSION
     *
     */
    public function afterSubmission( $entry, $form )
    {


	switch ($form['id'])
	{

	    case 1:

		$editentry	=GFAPI::get_entry($entry['id']);
		//make changes to it from new values in $_POST, this shows only the first field update
		$editentry[12]	=$this->_getNewRef();

		//update it
		$updateit=GFAPI::update_entry($editentry);
		break;
	}
    }

    /*
     * Update entry
     */
    public function updateEntry()
    {
	$entry_id	=$_POST['entry_id'];
	$fields		=$_POST['fields'];
	$action		=$_POST['action'];

	$entry=GFAPI::get_entry($entry_id);

	foreach ($fields as $field)
	{
	    $entry[$field['id']]=$field['value'];
	}


	$result=GFAPI::update_entry($entry);

	if ($action=="updateentrystatus")
	{
	    self::NotificationAfterUpdateEntry($entry_id);
	}

	wp_send_json($result);
    }
    /*
     * Send notification after entry approved
     */
    function afterEntryApproved()
    {

	if ($_POST['approved']==1)
	{
	    $entry_id	=$_POST['entry_slug'];

	    $event	='status_publish';
	    $entry	=RGFormsModel::get_lead($entry_id);
	    $form	=GFAPI::get_form(1);

	    $notifications		=GFCommon::get_notifications_to_send($event,$form,$entry);
	    $notifications_to_send	=array ();


	    //running through filters that disable form submission notifications
	    foreach ($notifications as $notification)
	    {
		if (apply_filters("gform_disable_notification_{$form['id']}",apply_filters('gform_disable_notification',false,$notification,$form,$entry),$notification,$form,$entry))
		{
		    //skip notifications if it has been disabled by a hook
		    continue;
		}

		$notifications_to_send[]=$notification['id'];
	    }
	
	    GFCommon::send_notifications($notifications_to_send,$form,$entry,true,$event);
	}
    }
    /*
     *
     * Send notification to admins after updating entry
     */
    static public function NotificationAfterUpdateEntry( $entry_id )
    {

	$event	='status_front';
	$form_id=1;
	$form	=RGFormsModel::get_form_meta($form_id);
	$entry	=RGFormsModel::get_lead($entry_id);

	$notifications		=GFCommon::get_notifications_to_send($event,$form,$entry);
	$notifications_to_send	=array ();

	//running through filters that disable form submission notifications
	foreach ($notifications as $notification)
	{
	    if (apply_filters("gform_disable_notification_{$form['id']}",apply_filters('gform_disable_notification',false,$notification,$form,$entry),$notification,$form,$entry))
	    {
		//skip notifications if it has been disabled by a hook
		continue;
	    }

	    $notifications_to_send[]=$notification['id'];
	}


	GFCommon::send_notifications($notifications_to_send,$form,$entry,true,$event);
    }
    /*
     * Update status Entry after sending proposal
     */
    static function updateStatusEntry( $entry_id )
    {

	$entry=GFAPI::get_entry($entry_id);

	if ($entry[13]==1)
	{
	    $entry[13]	=2;
	    $result		=GFAPI::update_entry($entry);
	    self::NotificationAfterUpdateEntry($entry_id);
	}
    }
    /*
     *
     * ADMIN
     */
    public function gw_add_manual_notification_event( $events )
    {
	$events['status_front']		=__('Statut d\'un lead modifié par un membre','tbc_plugin');
	$events['status_publish']	=__('Lead approuvé par l\'administrateur','tbc_plugin');
	$events['cron_triggered']	=__('Déclenchement de la tâche cron (process sending by WpMail function)','tbc_plugin');
	return $events;
    }
    /*
     * Replace custom Tag in notification fields
     */
    function my_custom_merge_tag_data( $data, $text, $form, $entry )
    {


	//{center:email}
	$field_id	=23; // Update this number to your field id number
	$field		=RGFormsModel::get_field($form,$field_id);
	$value		=is_object($field)?$field->get_value_export($entry):'';

	$centers=explode(",",$value);
	$list	=[];
	foreach ($centers as $center)
	{
	    $email_centre	=get_post_meta($center,"wpcf-email-centre",true);
	    $list[]		=$email_centre;
	}
	$email_list	=implode(",",$list);
	$data['center']	=array ('email'=>$email_list);


	//{members:email}
	$recipients	=TBC_Helper::getUsersByCenters($centers);
	$recipients	=implode(",",(array)$recipients);
	$data['members']=array ('email'=>$recipients);//{members:email}


	/* $recipients	=TBC_Helper::getCentersEmailsEntry($form,$entry);
	  $recipients	=implode(",",(array)$recipients);
	  $data['lead_tbc']	=array ('mails'=>$recipients); */


	//{admin:email}
	$admin_email	=get_option('tbc_options')['proposal_cc_address'];
	$data['admin']	=array ('email'=>$admin_email);


	return $data;
    }
    public function change_column_data( $value, $form_id, $field_id, $entry,
					$query_string )
    {
	//only change the data when form id is 1 and field id is 2
	if ($form_id==1)
	{

	    switch ($field_id)
	    {


		case 23:
		    $centers=explode(",",$value);
		    $args	=array ('post_type'	=>'centre',
			'post__in'	=>$centers);
		    $posts	=get_posts($args);
		    foreach ($posts as $center)
		    {
			$return[]=$center->post_title;
		    }
		    $value=implode('<br>',$return);

		    //   return $posts;
		    break;


		case 13:

		    $form	=GFAPI::get_form($form_id);
		    $field	=GFFormsModel::get_field($form,$field_id);
		    $value	=is_object($field)?$field->get_value_export($entry,$field_id,true):'';


		    break;
	    }
	}
	return $value;
    }
    public function after_submission( $entry, $form )
    {


	$type=rgar($entry,'15');
	if ($type=='tbc')
	{
	    $url=site_url().'/espace-membre/';
	    wp_redirect($url);
	    exit();
	}
    }
    public function ContainerMarkup( $field_container, $field, $form,
				     $css_class, $style, $field_content )
    {

	$id		=$field->id;
	$field_id	=is_admin()||empty($form)?"field_{$id}":'field_'.$form['id']."_$id";
	return '<div id="'.$field_id.'" class="'.$css_class.' form-group">{FIELD_CONTENT}</div>';
    }
    /*
     *
     *
     * Helpers
     *
     *
     *      */
    public function _getNewRef()
    {
	$date	=new DateTime("now");
	$year	=$date->format("Y");
	$form_id=1;

	//get last entry for contact form
	$search_criteria=array (
	/*  'field_filters'	=>array (
	  'mode'=>'any',
	  array (
	  'key'	=>'12',
	  'value'	=>''
	  )) */
	);
	$sorting	=array ('created'=>'DESC');
	$last_entry	=GFAPI::get_entries($form_id,$search_criteria,$sorting)[1];//get the last lead -1 because hook triggered after form submission


	$last_ref=$last_entry[12];

	//get counter
	$re	='/TBCW(\d{4})(.*)/';
	$str	=$last_ref;
	preg_match_all($re,$str,$matches,PREG_SET_ORDER,0);

	if ($matches)
	{
	    $last_counter	=$matches[0][2];
	    $last_counter	=$last_counter+1;
	    //set value ref id
	    $ref_id		="TBCW".$year.$last_counter;
	}
	else
	{
	    //first lead reference
	    $ref_id="TBCW".$year."1";
	}
	return $ref_id;
    }
    /*
     * get checkbox value
     */
    public function _get_checkbox_value( $entry, $field_id )
    {

	//getting a comma separated list of selected values
	$lead_field_keys=array_keys($entry);
	$items		=array ();
	foreach ($lead_field_keys as $input_id)
	{
	    if (is_numeric($input_id)&&absint($input_id)==$field_id)
	    {
		$items[]=GFCommon::selection_display(rgar($entry,$input_id),null,$entry['currency'],false);
	    }
	}
	//$value = GFCommon::implode_non_blank( ', ', $items );

	return $items;
    }

}
