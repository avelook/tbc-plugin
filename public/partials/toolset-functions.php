<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.avelook.fr
 * @since      1.0.0
 *
 * @package    Tbc
 * @subpackage Tbc/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Tbc
 * @subpackage Tbc/public
 * @author     avelook <raphael.thiry@avelook.fr>
 */
class Tbc_Toolset
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version )
    {

	$this->plugin_name	=$plugin_name;
	$this->version		=$version;
    }
    /*
     *
     *
     *
     */
    public function getLocation()
    {

	if (wp_verify_nonce($_POST['securite_nonce'],'securite-nonce'))
	{
	    $latitude	=$_POST['latitude'];
	    $longitude	=$_POST['longitude'];

	    global $wpdb;
	$sql=$wpdb->prepare(
	"SELECT DISTINCT
                map_lat.term_id,
		taxo.parent,
                map_lat.meta_value as locLat,
                map_lng.meta_value as locLong,
                 (
                     ( 6371 * acos( cos( radians(%f) ) * cos( radians( map_lat.meta_value ) ) * cos( radians( map_lng.meta_value ) - radians(%f) ) + sin( radians(%f) ) * sin( radians( map_lat.meta_value ) ) ) )

                 ) AS distance
	 FROM {$wpdb->prefix}terms as p
            INNER JOIN  {$wpdb->prefix}termmeta as map_lat ON p.term_id = map_lat.term_id
            INNER JOIN {$wpdb->prefix}termmeta as map_lng ON p.term_id = map_lng.term_id
            LEFT JOIN {$wpdb->prefix}term_taxonomy as taxo ON taxo.term_id = p.term_id
            WHERE 1 = 1
            AND map_lat.meta_key = 'wpcf-localisation-latitude'
            AND map_lng.meta_key = 'wpcf-localisation-longitude'
	    AND taxo.parent!=0 AND taxonomy='localisation'
            HAVING distance < 50
            ORDER BY distance ASC",$latitude,$longitude,$latitude);

	
	$nearbyLocation=$wpdb->get_results($sql);
	    echo json_encode($nearbyLocation[0]);
	}
    }
    /*
     *
     * Get breabdrumb products page
     */
    function getBreadcrumb( $attrs )
    {
	switch (get_post(get_the_ID())->post_type)
	{


	    case "bureau":
	    case "salle-de-reunion" :
	    case "espace-coworking" :
	    case "domiciliation" :

		$center_id	=get_post_meta(get_the_ID(),"_wpcf_belongs_centre_id",true);
		$center_post	=get_post($center_id);
		$center_title	=$center_post->post_title;
		$center_url	=get_post_permalink($center_post->ID);
		$page_title	=get_post(get_the_ID())->post_title;
		$searchurl	=get_post_permalink(29);


		$segments=[
		  //  ['url'=>$searchurl,'title'=>__('Tous nos centres','tbc_plugin')],
		    ['url'=>$center_url,'title'=>$center_title],
		    ['url'=>'','title'=>$page_title]
		];

		break;

	    case "centre":

		$center_post	=get_post(get_the_ID());
		$center_title	=$center_post->post_title;
		$searchurl	=get_post_permalink(29);

		$segments=[
		 //   ['url'=>$searchurl,'title'=>__('Tous nos centres','tbc_plugin')],
		    ['url'=>'','title'=>$center_title],
		];
		break;
	}

	
	$content='  <div class="breadc">';
	$i	=0;
	foreach ($segments as $segment)
	{
	    if ($i!=0)
	    {
		$content.=' > <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">';
	    }
	    $i++;

	    if ($segment['url']!='')
	    {
		$content.='<a itemprop="url" href="'.$segment['url'].'" title="'.$segment['title'].'">';
	    }
	    $content.='<span itemprop="title">'.$segment['title'].'</span>';
	    if ($segment['url']!='')
	    {
		$content.='</a>';
	    }
	    $content.='</span>';

	    }

	    $content.='</div>';

	return $content;
    }
    /*
     * tbc_search_product
     * Shortcode
     */
    function getSearchProduct( $attrs )
    {
	if (isset($_GET["wpv-wpcf-type-de-bureau"]))
	{
	    $fieldoptions=get_option("wpcf-fields")["type-de-bureau"]['data']['options'];

	    foreach ($fieldoptions as $key=> $fieldoption)
	    {
		if (isset($fieldoption['value']))
		{
		    if ($fieldoption['value']==$_GET["wpv-wpcf-type-de-bureau"])
		{
		$type=$fieldoption['title'];

		}
		}
	    }
	    return __('Bureaux','tbc_plugin').' '.$type;
	}

	elseif (isset($_GET["wpv-wpcf-nombre-de-personnes"]))
	{
	    $fieldoptions=get_option("wpcf-fields")["nombre-de-personnes"]['data']['options'];
	    foreach ($fieldoptions as $key=> $fieldoption)
	    {
		
		if (isset($fieldoption['value'])&&($fieldoption['value']==$_GET["wpv-wpcf-nombre-de-personnes"]))
		{
		$type=$fieldoption['title'];
		}
	    }
	    return __('Salles de réunion','tbc_plugin').' '.$type;
	}

	elseif (isset($_GET["wpv-wpcf-nombre-de-postes-coworking"]))
	{
	    $fieldoptions=get_option("wpcf-fields")["nombre-de-postes-coworking"]['data']['options'];
	    foreach ($fieldoptions as $key=> $fieldoption)
	    {

		if (isset($fieldoption['value'])&&($fieldoption['value']==$_GET["wpv-wpcf-nombre-de-postes-coworking"]))
		{
		$type=$fieldoption['title'];
		}
	    }
	    return __('Espaces coworking','tbc_plugin').' '.$type;
	}

	else
	{
	      return __('Domiciliations','tbc_plugin').' '.$type;
	}
    }
    /*
     * Shortcode TBC SLIDER
     */
    function getSearchSlider( $attrs )
    {
	$a	=shortcode_atts(array (
	    'type'=>false
	),$attrs);
	$type	=$a['type'];

	if ($type)
	{
	      $posts=$this->_getCurrentChildPosts($type);

	    switch ($type)
	    {

		case "bureau":
		    $field='wpcf-photos-bureaux';
		    break;

		case "espace-coworking":
		    $field='wpcf-photos-coworking';
		    break;

		case "salle-de-reunion":
		    $field='wpcf-photos-salles-de-reunion';
		    break;
	    }



	    $images_child	=get_post_meta($posts[0]->ID,$field,false)!=null?(array)get_post_meta($posts[0]->ID,$field,false):[];
	    $images_centre	=get_post_meta(do_shortcode('[wpv-post-id]'),'wpcf-photos-du-centre',false)!=null?(array)get_post_meta(do_shortcode('[wpv-post-id]'),'wpcf-photos-du-centre',false):[];
	    $images		=array_merge($images_child,$images_centre);
	}
	else
	{
	    $images=get_post_meta(do_shortcode('[wpv-post-id]'),'wpcf-photos-du-centre',false)!=null?(array)get_post_meta(do_shortcode('[wpv-post-id]'),'wpcf-photos-du-centre',false):[];
	}

	$content='                 <div class="inner_slider">  <div class="owl-carousel slider owl-theme">  ';

	//var_dump($images);


	foreach ($images as $image_url)
	    {
	    global $wpdb;
	    $attachment_id	=$wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';",$image_url))[0];
	    $src		=wp_get_attachment_image_src($attachment_id,'large');
	

	 if ($src[0]!='')
	    {
		$content.='<div class="owl-lazy item" data-src="'.$src[0].'")"><a class="carrousel-link" href="'.do_shortcode('[wpv-post-url]').'" target="_blank"></a></div>';
	    }
	}
	$content.='</div></div>';
	
	return $content;
    }
    /*
     * Shortcode
     * Get Child Product ID in Toolset syntax
     */
    function getChildId( $attrs )
    {
	$a=shortcode_atts(array (
	    'type'	=>'type'
	),$attrs);
	$type		=$a['type'];
	$child_posts	=$this->_getCurrentChildPosts($type);

	return $child_posts[0]->ID;
    }
    /*
     * get price value on result page
     */
    function getPriceValue( $attrs )
    {
	$a=shortcode_atts(array (
	    'type'=>'something else',
	),$attrs);
	$type		=$a['type'];
	$child_posts	=$this->_getCurrentChildPosts($type);

	switch ($type)
	{
	    case "bureau" :
		$content=$child_posts[0]->fields['price'];
		break;
	    case "salle-de-reunion" :
		$content=$child_posts[0]->fields['sdr-prix-a-partir-de'];
		break;

	    case "espace-coworking" :
		$content=$child_posts[0]->fields['coworking-prix-a-partir-de'];
		break;
	}


	return "<p class='text-primary'>".$content."</p>";
    }

    /*
     * Retrieve child post of current center
     */
    function _getCurrentChildPosts( $type )
    {
	

	switch ($type)
	{
	    case "salle-de-reunion" :
		$res=['key'=>'wpcf-nombre-de-personnes','value'=>$_GET['wpv-wpcf-nombre-de-personnes'],'default_value'=>'sdr-2-4-personnes'];
		break;

	    case "bureau" :
		$res=['key'=>'wpcf-type-de-bureau','value'=>$_GET['wpv-wpcf-type-de-bureau'],'default_value'=>'bureau-1-poste'];
		break;

	    case "espace-coworking" :
		$res=['key'=>'wpcf-nombre-de-postes-coworking','value'=>$_GET['wpv-wpcf-nombre-de-postes-coworking'],'default_value'=>'coworking-1-poste'];
		break;
	}
	


	//get child data
	$childargs	=array (
	    'post_type'	=>$type,
	    'meta_query'	=>array (
		array ('key'=>'_wpcf_belongs_centre_id','value'=>do_shortcode("[wpv-post-id]")),
		array ('key'=>$res['key'],'value'=>$res['value']!=''?$res['value']:$res['default_value']))
	);
	$child_posts	=types_child_posts('centre',$childargs);
	

	return $child_posts;
    }
    /*
     *
     * SHORTCODE
     *
     */
    function getPub()
    {

	//on  single page
	if (is_single())
	{


	    $parent=get_post_meta(do_shortcode("[wpv-post-id]"),"_wpcf_belongs_centre_id",true);

	    if ($parent==0)
	    {
		//center page
	    if (get_post_meta(do_shortcode("[wpv-post-id]"),'wpcf-ville',false)[0])
	    {
		$term_id=get_post_meta(do_shortcode("[wpv-post-id]"),'wpcf-ville',false)[0];
	    }
	    if (get_post_meta(do_shortcode("[wpv-post-id]"),'wpcf-arrondissement',false)[0])
	    {
		$term_id=get_post_meta(do_shortcode("[wpv-post-id]"),'wpcf-arrondissement',false)[0];
	    }
	    }
	    else
	    {
		//innerpages
		if (get_post_meta($parent,'wpcf-ville',false)[0])
	    {
		$term_id=get_post_meta($parent,'wpcf-ville',false)[0];
	    }
	    if (get_post_meta($parent,'wpcf-arrondissement',false)[0])
	    {
		$term_id=get_post_meta($parent,'wpcf-arrondissement',false)[0];
	    }
	    }
	}
	else
	{

	    // on search page

	    if ($_REQUEST["wpv-wpcf-pays"]!='')
	    {
	     $term_id=$_REQUEST["wpv-wpcf-pays"][0];
	    }
	    if ($_REQUEST["wpv-wpcf-ville"]!='')
	    {
		$term_id=$_REQUEST["wpv-wpcf-ville"][0];
	    }
	    if ($_REQUEST["wpv-wpcf-arrondissement"]!='')
	    {
		$term_id=$_REQUEST["wpv-wpcf-arrondissement"][0];
	    }

	}


	


	if ($term_id)
	{
	   
	    $content=types_render_termmeta('contenu-affiliation',array ('term_id'=>$term_id));

	    if ($content!='')
	    {
		$content="<h3>".__('Vous cherchez un hôtel ?','tbc_plugin')."</h3>".$content;
		return "<div class='associated'>".$content."</div>";
	    }
	}
    }
    /* TOOLSET CUSTOM FUNCTIONS */
//Filter Search Query : custom rules

    public function filterSearchQuery( $query, $view_setting, $view_id )
    {
	global $wpdb;


	if ($query['post_type'][0]=="centre"&&in_array($view_id,array (35,133,143)))
	{

	    $whereclause="";

	    switch ($view_id)
	    {

		//search view = BUREAUX
		case "35" :

		    $search=$_REQUEST["wpv-wpcf-type-de-bureau"];


		    if ($search!="")
		    {
			$whereclause=" AND b.meta_value ='".$search."'";
		    }


		    //get all centers with bureaux  that match conditions
		    $newquery	="SELECT a.meta_value FROM {$wpdb->prefix}postmeta as a
		    WHERE a.post_id IN (SELECT post_id FROM {$wpdb->prefix}postmeta as b
		    WHERE b.meta_key='wpcf-type-de-bureau' ".$whereclause.")
		    AND a.meta_key='_wpcf_belongs_centre_id'";
		    $centres	=$wpdb->get_results($newquery);


		    break;

		//search view = SDR
		case "133" :

		    $search=$_REQUEST["wpv-wpcf-nombre-de-personnes"];

		    if ($search!="")
		    {
			$whereclause=" AND b.meta_value ='".$search."'";
		    }

		    //get all centers with sdr  that match conditions
		    $newquery	="SELECT a.meta_value FROM {$wpdb->prefix}postmeta as a
		    WHERE a.post_id IN (SELECT post_id FROM {$wpdb->prefix}postmeta as b
		    WHERE b.meta_key='wpcf-nombre-de-personnes'  ".$whereclause.")
		    AND a.meta_key='_wpcf_belongs_centre_id'";
		    $centres	=$wpdb->get_results($newquery);

		    break;

		//search view = Coworking
		case "143" :

		    $search=$_REQUEST["wpv-wpcf-nombre-de-postes-coworking"];

		    if ($search!="")
		    {
			$whereclause=" AND b.meta_value ='".$search."'";
		    }

		    //get all centers with coworking spaces that match conditions
		    $newquery	="SELECT a.meta_value FROM {$wpdb->prefix}postmeta as a
		    WHERE a.post_id IN (SELECT post_id FROM {$wpdb->prefix}postmeta as b
		    WHERE b.meta_key='wpcf-nombre-de-postes-coworking' ".$whereclause.")
		    AND a.meta_key='_wpcf_belongs_centre_id'";
		    $centres	=$wpdb->get_results($newquery);
		//    var_dump($newquery);


		    break;
	    }



	    foreach ($centres as $centre)
	    {
		$ids[]=$centre->meta_value;
	    }


	    //unset($query['meta_query']);

	    if ($ids==null)
	    {
		$ids=array (0);
	    }

	

	    $query['post__in']=$ids;
	}


	return $query;
    }
    /*
     * dynamically populate select field from Types
     * Form edit or post page (admin + front)
     */
    public function populateSelectLocalisation( $options, $title, $type )
    {

	switch ($title)
	{

	    //get all countries
	    case 'pays':

		//$options	=array ();
		$options[]=array (
		    '#value'=>"",
		    '#title'=>__('Sélectionnez un pays','tbc_plugin'));

		$terms=Tbc_Helper::getCountries();
		foreach ($terms as $term)
		{
		    $options[]=array (
			'#value'=>$term->term_id,
			'#title'=>$term->name
		    );
		}
		break;

	    //get all cities
	    case 'ville':

		$post	=get_post();
		$pays	=get_post_meta($post->ID,'wpcf-pays',true);

		$options[]=array (
		    '#value'=>"",
		    '#title'=>__('Sélectionnez une ville','tbc_plugin'));

		//if country selected
		if ($pays)
		{
		    $terms=get_terms(
		    array (
			'taxonomy'	=>'localisation',
			'parent'	=>$pays,
			'hide_empty'	=>false
		    ));


		    foreach ($terms as $term)
		    {
			$options[]=array (
			    '#value'=>$term->term_id,
			    '#title'=>$term->name
			);
		    }
		}
		else
		{
		    $terms=Tbc_Helper::getCities();
		    foreach ($terms as $term)
		    {
			$options[]=array (
			    '#value'=>$term->term_id,
			    '#title'=>$term->name
			);
		    }
		}


		break;


	    //get arrondissements
	    case 'Arrondissement':

		$options=array_merge(array (
		    '#value'=>"",
		    '#title'=>__('Sélectionnez un arrondissement','tbc_plugin')),$options);

		$post	=get_post();
		$ville	=get_post_meta($post->ID,'wpcf-ville',true);

		if ($ville)
		{
		    $terms=get_terms(
		    array (
			'taxonomy'	=>'localisation',
			'parent'	=>$ville,
			'hide_empty'	=>false
		    ));


		    foreach ($terms as $term)
		    {
			$options[]=array (
			    '#value'=>$term->term_id,
			    '#title'=>$term->name
			);
		    }
		}



		break;
	}

	return $options;
    }
    /*
     * get cities AJAX after country
     */
    public function getCities()
    {

	$country=$_POST['country'];

	if (isset($country))
	{
	    $cities=get_terms(
	    array (
		'taxonomy'	=>'localisation',
		'parent'	=>$country,
		'hide_empty'	=>false
	    ));

	    foreach ($cities as $city)
	    {
		$values[]		=$city->term_id;
		$display_values[]	=$city->name;
	    }

	    array_unshift($values,"");
	    $values=implode(",",$values);

	    array_unshift($display_values,__('Toutes les villes','tbc_plugin'));
	    $display_values=implode(",",$display_values);

	    echo do_shortcode('[wpv-control  field="wpcf-ville" url_param="wpv-wpcf-ville" type="select" values="'.$values.'" display_values="'.$display_values.'"]');

	    return $cities;
	}
    }
    /*
     * get arrs AJAX after city
     */
    public function getArrs()
    {

	$city=$_POST['city'];

	if (isset($city))
	{
	    $arrs=get_terms(
	    array (
		'taxonomy'	=>'localisation',
		'parent'	=>$city,
		'hide_empty'	=>false
	    ));


	    foreach ($arrs as $arr)
	    {
		$values[]		=$arr->term_id;
		$display_values[]	=$arr->name;
	    }
	   

	    array_unshift($values,"");
	    $values=implode(",",$values);

	    array_unshift($display_values,__('Tous les arrondissements','tbc_plugin'));
	    $display_values=implode(",",$display_values);

	    echo do_shortcode('[wpv-control  field="wpcf-arrondissement" url_param="wpv-wpcf-arrondissement" type="select" values="'.$values.'" display_values="'.$display_values.'"]');

	    return $arrs;
	}
    }
    /*
     *  SEARCHBOX POPULATE LIST COUNTRIES AND CITIES !
     *  */


    /* prepopulate search select */
    public function listCountriesValues()
    {
	$values=Tbc_Helper::getCountries();

	$countries=array_map(function($value){
	    return $value->term_id;
	},$values);

	//array_unshift($countries,"");
	$countries=implode(",",$countries);
	return $countries;
    }
    /* prepopulate search select */
    public function listCountriesDisplayValues()
    {
	$values=Tbc_Helper::getCountries();

	$countries=array_map(function($value){
	    return $value->name;
	},$values);
	//array_unshift($countries,pll__('Pays'));
	$countries=implode(",",$countries);

	return $countries;
    }
    /* prepopulate search select with french cities */
    public function listFrCitiesValues()
    {
	if (isset($_GET['wpv-wpcf-pays'][0]))
	{
	    $parent=$_GET['wpv-wpcf-pays'][0];
	}
	else
	{
	    $parent=2;
	}

	$values	=get_terms(
	array (
	    'taxonomy'	=>'localisation',
	    'parent'	=>$parent,
	    'hide_empty'	=>false
	));
	$cities	=array_map(function($value){
	    return $value->term_id;
	},$values);

	array_unshift($cities,"");
	$cities=implode(",",$cities);

	return $cities;
    }
    /* prepopulate search select */
    public function listFrCitiesDisplayValues()
    {
	if (isset($_GET['wpv-wpcf-pays'][0]))
	{
	    $parent=$_GET['wpv-wpcf-pays'][0];
	}
	else
	{
	    $parent=2;
	}
	$values=get_terms(
	array (
	    'taxonomy'	=>'localisation',
	    'parent'	=>$parent,
	    'hide_empty'	=>false
	));

	$cities=array_map(function($value){
	    return $value->name;
	},$values);

	array_unshift($cities,__('Toutes les villes','tbc_plugin'));

	$cities=implode(",",$cities);
	return $cities;
    }
    /* prepopulate search select */
    public function listArrValues()
    {
	if (isset($_GET['wpv-wpcf-ville'][0])&&$_GET['wpv-wpcf-ville'][0]!='')
	{
	    $parent=$_GET['wpv-wpcf-ville'][0];

	    $values=get_terms(
	    array (
		'taxonomy'	=>'localisation',
		'parent'	=>$parent,
		'hide_empty'	=>false
	    ));

	    $arrs=array_map(function($value){
		return $value->term_id;
	    },$values);

	    array_unshift($arrs,"");
	    $arrs=implode(",",$arrs);


	    return $arrs;
	}
    }
    /* prepopulate search select */
    public function listArrDisplayValues()
    {

	if (isset($_GET['wpv-wpcf-ville'][0])&&$_GET['wpv-wpcf-ville'][0]!='')
	{
	    $parent=$_GET['wpv-wpcf-ville'][0];


	    $values=get_terms(
	    array (
		'taxonomy'	=>'localisation',
		'parent'	=>$parent,
		'hide_empty'	=>false
	    ));

	    $arrs=array_map(function($value){
		return $value->name;
	    },$values);

	    array_unshift($arrs,__('Tous les arrondissements','tbc_plugin'));

	    $arrs=implode(",",$arrs);


	    return $arrs;
	}
    }
    /*
     * Get center parent ID
     */
    function get_parent_id_func( $atts )
    {
	$child_ID	=$atts['id'];
	$parent_ID	=get_post_meta($child_ID,"_wpcf_belongs_centre_id",true);
	return $parent_ID;
    }
    /*
     * Get center ID Fr
     */
    function getCenterIdFr( $atts )
    {
	$id=icl_object_id($atts['id'],'centre',false,'fr');
	return $id;
    }
    /*
     * Get head  title (address page)
     */
 
    public function getHeadingAddress( $atts, $content='' )
    {
	static $city	=null;
	static $country	=null;
	$condition	=$atts['condition'];
	$value		=$atts['value'];
	switch ($condition)
	{
	    case 'city':
	    case 'country':
		if ($$condition!=$value)
		{
		    $$condition=$value;
		    return $content;
		}
		break;
	}
	return '';
    }
    public function getTermName( $atts )
    {
	$id		=$atts['id'];
	$taxonomy	=$atts['taxonomy'];
	$term		=get_term($id,$taxonomy,$output,$filter);
	return $term->name;
    }
    /*
     * Print btn reservation
     */
    public function getBtnReservation( $attr )
    {

	$a=shortcode_atts(array (
	    'type'=>'something else',
	),$attr);

	$type=$a['type'];

	$array=[
	    'salle-de-reunion'=>
	    [
		'search_var'	=>'wpv-wpcf-nombre-de-personnes',
		'get_var'	=>'wpcf-nombre-de-personnes',
		'child_key'	=>'wpcf-planyo-sdr-shortcode-attributes',
		'parent_key'	=>'wpcf-planyo-centre-sdr-attributes',
	    ],
	    'bureau'		=>
	    [
		'search_var'	=>'wpv-wpcf-type-de-bureau',
		'get_var'	=>'wpcf-type-de-bureau',
		'child_key'	=>'wpcf-planyo-bureau-shortcode-attributes',
		'parent_key'	=>'wpcf-planyo-centre-bureau-attributes',
	    ],
	    'coworking'		=>
	    [
		'search_var'	=>'wpv-wpcf-nombre-de-postes-coworking',
		'get_var'	=>'wpcf-nombre-de-postes-coworking',
		'child_key'	=>'wpcf-planyo-coworking-shortcode-attributes',
		'parent_key'	=>'wpcf-planyo-centre-coworking-attributes',
	    ]
	];

	$search_var=$array[$type]['search_var'];


	if ($_GET[$search_var]!='')
	{



	    $childargs=array (
		'post_type'	=>$type,
		'meta_query'	=>
		array (array ('key'=>'_wpcf_belongs_centre_id','value'=>get_the_ID())),
		array ('key'=>$array[$type]['child_key'],'value'=>'','compare'=>'!='),
		array (
	      'key'	=>$array[$type]['get_var'],'value'	=>''.$_GET[$search_var].'')
	    );

	    $child_post=get_posts($childargs)[0];


	    if ($child_post->ID)
	    {
		$vars=get_post_meta($child_post->ID,$array[$type]['child_key'],true);
		
	    }
	}
	else
	{
	    $vars=get_post_meta(get_the_ID(),$array[$type]['parent_key'],true);
	}

	if ($vars)
	{
	    return "<a target='blank' class='btn btn-light third-part-service btn-block' href='".site_url()."/reservez-votre-salle-de-reunion?".$vars."' >".do_shortcode("[wpml-string context = 'tbc_view']Réserver[/wpml-string]")."</a>";
	}
    }
    /*
     * Redirect to center page instead page list if result=1*/
    public function redirectCenter()
    {

		global $wp_query;

	switch (get_the_ID())
	{
	    case 46 :
		if (count(get_view_query_results(35))==1)
		{
		    wp_redirect(get_permalink(get_view_query_results(35)[0]->ID));
		}
		break;

	    case 137 :
		if (count(get_view_query_results(133))==1)
		{
		    wp_redirect(get_permalink(get_view_query_results(133)[0]->ID));
		}
		break;

	    case 145 :
		if (count(get_view_query_results(143))==1)
		{
		    wp_redirect(get_permalink(get_view_query_results(143)[0]->ID));
		}
		break;
	    case 147 :
		if (count(get_view_query_results(149))==1)
		{
		    wp_redirect(get_permalink(get_view_query_results(149)[0]->ID));
		}
		break;
	}
    }

    public function count_child_posts( $args, $content='' )
    {
	$types	=['bureau','salle-de-reunion','coworking','domiciliation'];
	$result	=0;
	foreach ($types as $type)
	{
	    $child_posts	=types_child_posts($type);
	    $count		=count($child_posts);
	    $result		=$result+$count;
	}

	return (int)$result;
    }
    /**
     * Filter the custom inner shortcodes array to add custom shortcodes
     * Works inside GView shortcodes for search
     */
    public function innerToolsetShortcodes( $shortcodes )
    {
	$shortcodes[]	='frcities_display_values';
	$shortcodes[]	='frcities_values';
	$shortcodes[]	='country_values';
	$shortcodes[]	='country_display_values';
	$shortcodes[]	='frarr_values';
	$shortcodes[]	='frarr_display_values';
	$shortcodes[]	='get_parent_id';
	$shortcodes[]	='getBtnReservation';
	$shortcodes[]	='get_child_id';
	$shortcodes[]	='tbc_slider';
	$shortcodes[]	='head_address';
	$shortcodes[]	='count_child_posts';
	return $shortcodes;
    }

}
