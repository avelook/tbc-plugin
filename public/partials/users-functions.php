<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tbc_Users
{

    /*
     * Add custom field centers to user profile
     */
    public function extra_user_profile_fields($user)
    {

        if (!in_array("administrator", $user->roles)) {
            ?>
	    	    <h3><?php __("Extra profile information", "tbc_plugin");?></h3>

			<table class="form-table">
				<tr>
	    	    				    <th><label for="centers"><?php __("Centers", 'tbc_plugin');?></label></th>
	    				    <td>

							<?php
$centers = Tbc_Helper::getCenters();
            foreach ($centers as $center) {
                $checked = "";
                if (in_array($center->ID, explode(',', get_user_meta($user->id, 'centers')[0]))) {
                    $checked = "checked";
                }
                ?>
	    						<div class="form-group">

	    							<input type="checkbox" name="centers[]" id="center-<?php echo $center->ID; ?>" value="<?php echo $center->ID; ?>" <?php echo $checked; ?>>
	    							<label class="description" for="center-<?php echo $center->ID; ?>"><?php echo $center->post_title ?></label>
	    						</div>
	    <?php
}
            ?>

					</td>
				</tr>

			</table>
	<?php
}
    }
    /*
     * Save custom field centers to user profile
     */
    public function save_extra_user_profile_fields($user_id)
    {
        $user = new WP_User($user_id);
        if (!current_user_can('edit_user', $user_id) || (in_array("administrator", $user->roles))) {
            return false;
        }
        $centers = implode(",", $_POST['centers']);
        update_user_meta($user_id, 'centers', $centers);
    }
//Prevent access admin panel non administrator users
    public function prevent_admin_access()
    {
        /* if (false!==strpos(strtolower($_SERVER['REQUEST_URI']),'/wp-admin'&&false==strpos(strtolower($_SERVER['REQUEST_URI']),'/wp-admin/admin-ajax.php')
    )&&!current_user_can('administrator'))
    wp_redirect(home_url()); */
    }

    /*
     * remove admin bar if subscriber
     */
    public function remove_admin_bar()
    {
        /* if (!current_user_can('administrator')&&!is_admin())
        {
        show_admin_bar(false);
        } */

        $user = wp_get_current_user();
        if (in_array("subscriber", $user->roles)) {
            show_admin_bar(false);
        }
    }

}
