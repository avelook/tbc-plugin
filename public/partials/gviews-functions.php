<?php
/**
 *
 * @package    Tbc
 * @author     avelook <raphael.thiry@avelook.fr>
 */
class Tbc_GViews
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;
    }
    public function gravityview_Entries($return, $args)
    {

        //get all entries after criteria
        $parameters = GravityView_frontend::get_view_entries_parameters($args, 1);
        $return     = $this->_filterEntries($return, $parameters, $args);

        return $return;
    }
    /*     * **********
     * GRAVITY VIEW
     *  SEARCH VIEW + ENTRY VIEW
     * HOOK : gravityview_search_criteria
     *  ********** */
    public function my_gv_custom_role_filter($criteria)
    {
        // SEARCH VIEW  Multiple entries
        //get current user
        $current_user = wp_get_current_user();
        $user_id      = $current_user->ID;
        $user_centers = Tbc_Helper::getUserCenters($user_id);

        //redirect to homepage with message if subscriber is not associated to a center
        if ($user_centers == false && in_array('subscriber', (array) $current_user->roles)) {
            $redirect = add_query_arg('msg', 'no-center', get_home_url());
            wp_redirect($redirect);
            exit;
        }

        // INNER VIEW Single Entry
        if ($criteria['context_view_id'] != null) {

            global $wp_query;
            $entry_id = $wp_query->get('entry');
            $entry    = GFAPI::get_entry($entry_id);
            $field_id = 23;
            $form     = GFAPI::get_form(1);
            $field    = RGFormsModel::get_field($form, $field_id);
            $value    = is_object($field) ? $field->get_value_export($entry) : '';
            $list     = explode(', ', $value);

            if (in_array('subscriber', (array) $current_user->roles) && array_intersect($list, $user_centers) == false) {
                $redirect = add_query_arg('msg', 'no-access', get_home_url());
                wp_redirect($redirect);
                exit;
            }

            if ($this->getEntryHandleStatus($entry_id)['status'] == "expired" && in_array('subscriber', (array) $current_user->roles)) {
                $redirect = add_query_arg('msg', 'no-access-lead', get_home_url());
                wp_redirect($redirect);
                exit;
            }
        } else {

            if (in_array('subscriber', (array) $current_user->roles)) {
                /* //var_dump($criteria['search_criteria']);
            $orig=$criteria['search_criteria']['field_filters'];

            //limitation : we cannot add any + all mode in the same time
            $filter['mode']='any';

            //    $filter[]=$criteria['search_criteria']['field_filters'][0];

            foreach ($user_centers as $center)
            {
            $filter[]=array ('key'=>23,'operator'=>'contains','value'=>'"'.$center.'"');
            }

            $criteria['search_criteria']['field_filters']=$filter;

            /* $crit['search_criteria']['field_filters'][0]    =$filter;
            $crit['search_criteria']['field_filters'][1]    =$orig;
            //$criteria                =\GV\GF_Entry_Filter::merge_search_criteria($search_criteria1,$search_criteria2);
            echo '<pre>';
            var_dump($crit);
            echo '</pre>';
            $criteria                    =$crit; */
            }
        }

        return $criteria;
    }
    /*
     * RECALL MEMBERS LEADS STATUS (via REST API WP)
     * url : team-business-centers.com/wp-json/tbc/v1/recall?pass=16@Opd
     */
    public function recallCronMembers()
    {

        register_rest_route('tbc/v1', '/recall', array(
            'methods'  => 'GET',
            'callback' => array($this, '_sendRecall'),
        ));
    }
    /*
     * Function that filter returned entries after Search criteria and that modify entry list and pagination !!
     * Fix gravity view limitations
     */
    public function _filterEntries($return, $parameters, $args = null)
    {

        $current_user = wp_get_current_user();

        if (in_array('subscriber', (array) $current_user->roles)) {
            $parameters['paging']['page_size'] = 999999999999999;
            $parameters['paging']['offset']    = 0;
            $entries                           = \GV\Mocks\GravityView_frontend_get_view_entries($args, 1, $parameters, 999);

            //params
            $offset    = $return['paging']['offset'];
            $page_size = $return['paging']['page_size'];

            //get user centers
            $user_id      = $current_user->ID;
            $user_centers = Tbc_Helper::getUserCenters($user_id);

            $newentries = [];
            $counter    = 0;

            foreach ($entries[0] as $entry) {
                $list = (array) json_decode($entry['23']);

                if ($entry['is_approved'] == 1 && array_intersect($list, $user_centers)) {
                    $counter++;
                    if ($counter > $offset && $counter <= ($offset + $page_size)) {
                        $newentries[] = $entry;
                    }

                }
            }

            $return['entries'] = $newentries;
            $return['count']   = $counter;

        }

        return $return;
    }
    /*
     * function called by cron task
     */
    public function _sendRecall()
    {
        //recall
        if (isset($_GET['pass']) && $_GET['pass'] == "16@Opd") {

            $event         = 'cron_triggered';
            $form          = GFAPI::get_form(1);
            $notifications = GFCommon::get_notifications($event, $form);
            $delay         = get_option('tbc_options', array())['delay_lead'];

            //get entries
            $search_criteria = array(
                'status'        => 'active',
                'field_filters' =>
                [
                    ['key'     => '13',
                        'operator' => 'in',
                        'value'    => array(1, 2),
                    ],
                ],
            );

            $sorting = array('created' => 'DESC');
            $null    = null;
            $paging  = array('offset' => 0, 'page_size' => 9999999); //important

            $entries = GFAPI::get_entries($form['id'], $search_criteria, $sorting, $paging, $null);

            foreach ($entries as $entry) {
                if ($entry['is_approved'] == 1) {

                    //non sent
                    if ($entry[13] == 1) {

                        $expiration_date = $this->getExpirationDate($entry);

                        //expiration in 2 days
                        $today   = new DateTime('now');
                        $in2days = $today->modify('+ 2 days');

                        if ($expiration_date->format('Y-m-d') == $in2days->format('Y-m-d')) {
                            $entries_expired_in2days[] = $entry;
                        }

                        //expiration today
                        $today = new DateTime('now');
                        if ($expiration_date->format('Y-m-d') == $today->format('Y-m-d')) {
                            $entries_expired_today[] = $entry;
                        }
                    }

                    if ($entry[13] == 2) {

                        //get only entries that are not expired
                        $expiration_date = $this->getExpirationDate($entry);
                        $sent_date       = new DateTime($entry['26']);

                        if ($sent_date->format("Y-m-d") <= $expiration_date->format("Y-m-d")) {
                            $entries_not_updated[] = $entry;
                        }

                    }

                }
            }

            //get notifications and prepare sending
            foreach ($notifications as $notification) {

                /*
                 * GET LEADS OUTDATED IN 2 DAYS
                 */
                if ($notification['id'] == "5bb4e126496f8") {

                    $this->prepareSendMail($entries_expired_in2days, $notification);
                }

                /*
                 * GET LEADS OUTDATED TODAY
                 */
                if ($notification['id'] == "5bb5f564269e8") {

                    $this->prepareSendMail($entries_expired_today, $notification);
                }

                /*
                 * GET LEADS WITH NO UPGRADED STATE
                 */
                $today = new DateTime('now');
                if ($notification['id'] == "5a7494bb302c6" && in_array($today->format("d"), [01, 10, 20])) {

                    var_dump('entries_not_updated');
                    var_dump($entries_not_updated);
                    $this->prepareSendMail($entries_not_updated, $notification);
                }
            }
        }

    }
    /*
     * prepare sending mail process
     */
    public function prepareSendMail($entries, $notification)
    {

        if (count($entries) > 0) {
            //for each entry, get emails related (center + users) and combine
            $recipients = [];
            $list       = [];
            foreach ($entries as $entry) {
                $e          = Tbc_Helper::getCentersEmailsEntry($entry);
                $recipients = array_merge($e, $recipients);
                $recipients = array_unique($recipients);
                //$list[]        =['entry'=>$entry,'emails'=>$e];
            }

            if ($_GET['test'] == 1) {

                var_dump(count($entries));
                var_dump($recipients);
            }

            // return new WP_REST_Response($recipients,200);
            if (!empty($recipients) && !isset($_GET['test'])) {
                $this->sendEmail($recipients, $notification);
            }
        }
    }

    /*
     * Send recall mail
     */
    public function sendEmail($recipients, $notification)
    {

        global $phpmailer;

        /* $recipients=implode(",",$recipients); */

        $to = $notification['to'];

        foreach ($recipients as $recipient) {
            $headers[] = 'Bcc: ' . $recipient;
        }
        //  $headers[]    ='Cc: '.$notification['bcc'].' <'.$notification['bcc'].'>';
        $subject   = $notification['subject'];
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        //  $headers[]    ='Cc: '.$notification['bcc'].' <'.$notification['bcc'].'>';
        $headers[] = 'Reply-To: ' . $notification['replyTo'] . ' <' . $notification['replyTo'] . '>';
        $headers[] = 'From: ' . $notification['fromName'] . '<' . $notification['from'] . '>';
        $body      = $notification['message'];

        $is_success = wp_mail($to, $subject, $body, $headers);

        if ($is_success) {
            echo "Message sent!";
        } else {
            echo "Mailer Error: " . $phpmailer->ErrorInfo;
            mail('raphael.thiry@avelook.fr', 'erreur relance site TBC', json_encode($phpmailer->ErrorInfo));
        }

        //exit();
    }
    /*
     * Custom field values !
     */
    public function customEntryFieldValue($value, $field, $lead, $form)
    {

        $thevalue = $value;

        //get current user
        $current_user = wp_get_current_user();

        //hide to subscriber if status <2 (show icon)
        if (in_array('subscriber', (array) $current_user->roles)) {

            if ($lead[13] < 2) {

                if (in_array($field->id, array(3, 10))) {
                    $value = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
                }
            }
        }

        //STATUS SELECTBOX CONDITIONNAL RENDER
        if ($field->id == 13) {

            $choices = $field['choices'];

            //status not equal to"proposal pending"
            if (in_array($value, [1, 6]) && (in_array('subscriber', (array) $current_user->roles))) {
                $val = array_filter($choices, function ($n) {

                    if ($n['value'] == 1) {

                        return $n['text'];
                    }
                });

                $value = "<div class='badge badge-danger'>" . $val[0]['text'] . "</div>";

                /* add new state */
                $selectbox = "<select class='badge entity_status no-niceselect'>";
                foreach ($choices as $choice) {
                    if (in_array($choice['value'], array(1, 6))) {

/*
if ($choice['value'] == $thevalue) {

$selected = "selected='selected'";
}*/

                        $selectbox .= "<option entry_id='" . $lead['id'] . "' value='" . $choice['value'] . "'  " . ($choice['value'] == $thevalue ? "selected='selected'" : '') . ">" . $choice['text'] . "</option>";
                    }
                }
                $selectbox .= "</select>";
                $value = $selectbox;

            } else {

                if ($value == 5 && (in_array('subscriber', (array) $current_user->roles))) {
                    $attr = "disabled";
                }
                $selectbox = "<select " . $attr . " class='badge entity_status no-niceselect'>";

                foreach ($choices as $choice) {
                    if ((in_array('subscriber', (array) $current_user->roles)) && in_array($choice['value'], array(6))) {
                        continue;
                    }

                    if ($choice['value'] == 1 && $thevalue > 1) {
                        continue;
                    }

                    $selected = "";

                    if ($choice['value'] == $thevalue) {

                        $selected = "selected";
                    }
                    $selectbox .= "<option entry_id='" . $lead['id'] . "' value='" . $choice['value'] . "'  " . $selected . ">" . $choice['text'] . "</option>";
                }
                $selectbox .= "</select>";
                $value = $selectbox;

            }
        }

        return $value;
    }
/*     * **********
 * GRAVITY VIEW
 * ENTRY
 *  ********** */
    public function showUploadField($view_id)
    {

        //get the field
        $form  = GFAPI::get_form(1);
        $field = GFFormsModel::get_field($form, 6);

        global $wp_query;
        $entry_id = $wp_query->get('entry');
        $entry    = GFAPI::get_entry($entry_id);

        //defaults
        $default_message = __('Bonjour,<br><br>Nous vous remercions pour votre demande sur le site de TBC. Nous sommes ravis de vous faire part de notre proposition commerciale que vous trouverez en pièce-jointe.<br><br>Toute notre équipe se tient à votre entière disposition pour tout complément d\'information.<br><br>Dans l\'attente de votre retour<br><br>', 'tbc_plugin');
        $default_message .= '<img src="' . get_template_directory_uri() . '/assets/images/logo-small.png" data-mce-src="' . get_template_directory_uri() . '/assets/images/logo-small.png" />';
        /* if entry id exists */
        if ($entry_id) {

            //get the html content
            $content = $field->adminLabel;

            //get current user
            $current_user = wp_get_current_user();
            $user_email   = $current_user->user_email;

            //get center
            $center_name  = get_post($entry[8])->post_title;
            $center_email = get_post_meta($entry[8], 'wpcf-email-centre', true);
            $repicient    = $entry[3];

            echo "<div class='proposal_box'>";
            //render field
            $tbc_proposal = rgar($entry, 29);
            $date         = new DateTime(rgar($entry, 26));
            $date         = $date->setTimezone(new \DateTimeZone('Europe/Paris'));

            if ($tbc_proposal) {
                echo '<a class="" href="' . $tbc_proposal . '" target="_blank"><img src="' . get_template_directory_uri() . '/assets/images/export-pdf.png" width="30"/><p>' . __('Visualisez votre proposition commerciale TBC', 'tbc_plugin') . '</a></p>';
            }
            if (rgar($entry, 26) != '') {
                echo ' <p>' . __('Date d\'envoi de la proposition : ', 'tbc_plugin') . $date->format('d/m/Y H:i:s') . '</p>';
            } else {
                echo ' <p>' . __('Votre proposition n\'a pas encore été envoyée', 'tbc_plugin') . '</p>';
            }

            // PREPARE MODALCONTENT
            $content = '<form id="proposalForm" url="' . site_url() . '/wp-admin/admin-ajax.php" action="sendproposal">';

            if ($entry[13] > 3) {
                $content .= '<div class="form-group"><label>' . __('A :', 'tbc_plugin') . '</label><br>' . $repicient . '</div>';
            }

            $content .= '<div class="form-group"><label>' . __('De (adresse mail)', 'tbc_plugin') . '</label><input class="form-control required email" name="from_email"  value="' . $user_email . '" type="text"></div>';
            $content .= '<div class="form-group"><label>' . __('CC (adresses mails en copie, séparées par des virgules)', 'tbc_plugin') . '</label><input class="form-control" name="cc"  value="" type="text"></div>';
            $content .= '<div class="form-group"><label>' . __('Sujet ', 'tbc_plugin') . '</label><input type="text" name="subject" class="form-control required" required value="' . __('Notre proposition suite à votre demande sur le site tbc', 'tbc_plugin') . '"/></div>';
            $content .= '<div class="form-group"><label>' . __('Message ', 'tbc_plugin') . '</label><textarea class="tinymce" name="message" ud="msg" class="form-control required" required col="10" row="100" style="height:200px" >' . $default_message . '</textarea></div>';
            //    $content.='<div class="form-group"><input type="hidden" required name="from_name" value="'.$center_name.'"/></div>';

            $content .= '<div id="proposal" class="bg-light p-3  mb-3 form-group">';
            $content .= '<h3>' . __('Uploadez votre proposition (PDF)', 'tbc_plugin') . '</h3><p>' . __('Seuls les fichiers pdf <strong>au format portrait</strong> sont autorisés (maximum 5Mo). Votre proposition commerciale sera fusionnée à 2 pages TBC', 'tbc_plugin') . '</p>';

            $content .= '
            <span class="btn btn-default btn-file btn-block"><i class="fa fa-upload"></i>Sélectionnez un fichier<input id="proposalupload" type="file" name="files" data-url="' . site_url() . '/wp-admin/admin-ajax.php" ></span>
            <input type="hidden" name="securite_nonce" value="' . wp_create_nonce('securite-nonce') . '"/>
                                                    <input type="hidden" name="action" value="sendproposal"/>
            <input type="hidden" name="entry_id" value="' . get_query_var('entry') . '"/>

                <div class="progress">
    <div class="bar" style="width: 0%;"></div>
';

            $content .= '<div class="files"">';
            if ($entry[29]) {
                $content .= __('Votre proposition :', 'tbc_plugin') . '<a href="' . $entry[29] . '" target="_blank">' . pathinfo($entry[29])['basename'] . '</a>';
                $content .= '<button class="delete remove-proposal"><i class="fa fa-times"></i></button>';
            }
            $content .= '</div>';
            /*   if (current_user_can('administrator'))
            {
            $content.='<a class="ajax-test">test</a>';
            } */

            $content .= '<input type="hidden" value="' . pathinfo($entry[29])['basename'] . '"  name="attachment_name" required="required" />';

            $content .= '</div>';
            $content .= '</div>';

            $attachments = json_decode(rgar($entry, 31));
            $filelist    = '';

            if (is_array($attachments)) {
                foreach ($attachments as $index => $file) {
                    $filelist .= '<div class="uploadedfile"><a href="' . $file->url . '" target="_blank">' . $file->name . '</a><button href="#" class="delete" action="deleteFile" pos="' . $index . '" field_id="31" data-type="DELETE" data-url="' . $file->deleteUrl . '" title="Delete"><i class="fa fa-times"></i></button><input type="hidden" name="uploadedfiles[]" value="' . $file->name . '"/></div>';
                }
            }
            $content .= '<div id="attachments" class="bg-light p-3 form-group">
        <h3>Ajoutez des pièces jointes</h3>
         <div class="upload-control input-group flex-column"><span class="btn btn-default btn-file"><i class="fa fa-upload"></i>Sélectionnez un fichier<input class="ajaxupload single-upload"  action="uploadFile" data-url="' . site_url() . '/wp-admin/admin-ajax.php" type="file" field_id="31" value=""/></span>
        <div class="progress">
        <div class="bar" style="width: 0%;"></div>
        </div>
        <div class="files">' . $filelist . '</div>
        </div>


         </div>';

            $content .= '<div id="submit_area" class="form-group"><button  class="btn btn-block btn-primary" type="submit" onclick="return confirm(\'Êtes-vous sûr de vouloir continuer ?\')">Envoyer</button></div>';
            $content .= '</form>';

            //render modalbox
            echo
            '<!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#proposalModal">' . __('Envoyez votre proposition', 'tbc_plugin') . '</button></div>
            <!-- Modal -->
            <div id="proposalModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
            <h2 class="modal-title">' . __('Envoyez votre proposition', 'tbc_plugin') . '</h2>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            ' . $content . '
            </div>

            </div>

            </div>
            </div>';

        }
    }

/* get config tbc shortcode */
    public function getTbcConfig($attr)
    {

        $a = shortcode_atts(array(
            'param' => 'something else',
        ), $attr);

        $param = $a['param'];

        return get_option('tbc_options', array())[$param];
    }
/*
 * get Stars Standing pic // shortcode
 */
    public function getTbcStars($attr)
    {

        $a = shortcode_atts(array(
            'value' => 'something else',
        ), $attr);

        $param = $a['value'];

        $output = '';
        for ($i = 0; $i < $param; $i++) {
            $output .= '<img src="' . get_template_directory_uri() . '/assets/images/pic-star.jpg"/>';
        }
        return $output;
    }
/*
 * Hide option values in searchbox GView if current user is subscriber
 */
    public function beforeEntry($view_id)
    {
        $current_user = wp_get_current_user();

        if (in_array('subscriber', (array) $current_user->roles)) {
            echo '<style>.gv-search-box option[value="6"],.gv-search-box option[value="5"]{display:none}</style>';
            echo '<script>jQuery(function()
        {
        /*jQuery(".gv-widget-search").remove();*/
        jQuery(".gv-search-box.gv-search-field-select:eq(0)").remove();
        })</script>';
        }

    }
/*
 * Modify field value gview
 */
    public function modifyValuesEspaceMembre($output, $field_settings,
        $format = 'html', $field_data) {

        $entry        = GFAPI::get_entry($field_data['entry']['id']);
        $entry_status = $entry[13];
        $current_user = wp_get_current_user();

        if ($format['id'] == 'entry_link') {

            if ($this->getEntryHandleStatus($field_data['entry']['id'])['status'] == "expired" && in_array('subscriber', (array) $current_user->roles)) {
                /* $output=' <a data-toggle="tooltip" title="'.__('Ce lead a été bloqué car la période de traitement est dépassée','tbc_plugin').'"><i class="fa fa-ban"></i></a>
                '; */
                $output = '';
            }
        }

        // sending date format
        if ($format['id'] == '26') {
            if ($output != "") {

                $output = new DateTime($output);
                $output = $output->setTimezone(new \DateTimeZone('Europe/Paris'));
                $output = $output->format("d/m/Y H:i:s");
            }
        }
        //get by tbc
        if ($format['id'] == '27.1') {

            $entry_id = $field_data['entry']['id'];

            if ($output != 1) {
                $output = '<a class="updated_entry_field" href="#" data-value="0" data-entry-slug="' . $entry_id . '" data-field-id="27"><i class="fa fa-close" aria-hidden="true"></a>';
            } else {

                $output = '<a class="updated_entry_field" href="#"  data-value="1" data-entry-slug="' . $entry_id . '" data-field-id="27"><i class="fa fa-check" aria-hidden="true"></a>';
            }
        }

        //VAT Amount
        if ($format['id'] == 24) {
            $output = "<input class='ftup' type='text' value='" . $output . "'/>";
        }

        //attachments files
        if ($format['id'] == 31) {
            $uploadedfiles = json_decode($field_data['value']);
            $filelist      = '';
            foreach ($uploadedfiles as $index => $file) {
                $filelist .= '<a href="' . $file->url . '" target="_blank">' . $file->name . '</a><button href="#" class="delete" action="deleteFile" pos="' . $index . '" field_id="' . $field_data['field_id'] . '" data-type="DELETE" data-url="' . $file->deleteUrl . '" title="Delete"><i class="fa fa-times"></i></button>';
            }

            $output = "<div class='upload-control'>
        <span class='btn btn-default btn-file'><i class='fa fa-upload'></i>Sélectionnez un fichier<input class='ajaxupload single-upload' action='uploadFile'  data-url='" . site_url() . "/wp-admin/admin-ajax.php' type='file' field_id='" . $field_data['field_id'] . "' value=''/></span>
        <div class='progress'>
        <div class='bar' style='width: 0%;'></div>
        </div>
        <div class='files'>" . $filelist . "</div>
        </div>";
        }

        //invoice field
        if ($format['id'] == 25) {

            if (in_array($entry_status, [4, 5])) {
                $file     = $field_data['value'];
                $filename = pathinfo($field_data['value'])['basename'];
                if ($filename) {
                    $link = '<a href="' . $file . '" target="_blank">' . $filename . '</a><button href="#" class="delete" action="deleteFile" field_id="' . $field_data['field_id'] . '" data-type="DELETE" data-url="' . site_url() . '/wp-admin/admin-ajax.php?file=' . $filename . '" title="Delete"><i class="fa fa-times"></i></button>';
                }
                $output = "<div class='form-group'>
        <span class='btn btn-default btn-file'><i class='fa fa-upload'></i>Sélectionnez un fichier<input class='simpleajaxupload single-upload' action='uploadFile'  data-url='" . site_url() . "/wp-admin/admin-ajax.php' type='file' field_id='" . $field_data['field_id'] . "' value=''/>
        </span><div class='progress'>
        <div class='bar' style='width: 0%;'></div>
        </div>
        <div class='files'>" . $link . "</div>
        </div>";
            }
        }

        //centers list
        if ($format['id'] == 23) {
            $entries = $field_data['entry'];
            foreach ($entries as $key => $t) {
                if ($key == 23) {
                    $centers = json_decode($t);
                }
            }

            if ($centers) {

                //show only my centers if subscriber
                if (in_array('subscriber', (array) $current_user->roles)) {
                    $user_id      = $current_user->ID;
                    $user_centers = TBC_Helper::getUserCenters($user_id);
                    $centers      = array_intersect($user_centers, $centers);
                }

                $args  = array('post_type' => 'centre', 'post__in' => $centers);
                $posts = get_posts($args);

                foreach ($posts as $center) {
                    $op[] = $center->post_title;
                }
                if (is_array($op)) {

                    $output = '<ul>';
                    foreach ($op as $c) {
                        $output .= '<li>' . $c . '</li>';
                    }
                    $output .= '</ul>';
                    //  $output=implode(",",$op);
                }
            }
        }

        return $output;
    }
/*
 * Display Outdated Message in Custom field
 */
    public function getOutdatedMsg($params, $content = null)
    {
        extract(shortcode_atts(array(
            'entry_id' => false,
        ), $params));

        $res = $this->getEntryHandleStatus($entry_id);

        switch ($res['status']) {
            case "expired":
                echo '<em class="badge badge-secondary">' . __("Expiré", 'tbc_plugin') . '</em>';
                break;

            case "handled":
                echo '';
                break;

            case "pending":
                if ($res['diff'] > 1) {
                    echo '<em>' . sprintf(__("Expire dans %s jours", 'tbc_plugin'), $res['diff']) . ' </em><br><sub>(le ' . $res['last']->format('d/m/Y') . ')</sub>';
                    //    echo '<i class="fa fa-clock-o" aria-hidden="true"></i>';
                } elseif ($res['diff'] == 1) {
                    echo '<em>' . __("Expire dans 1 jour", 'tbc_plugin') . ' </em><br><sub>(le ' . $res['last']->format('d/m/Y') . ')</sub>';
                } elseif ($res['diff'] == 0) {
                    echo '<em>' . __("Expire aujourd'hui", 'tbc_plugin') . '</em>';
                }
                break;
        }
    }

    /*
    Check Entry Status : expired, pending...
     */
    public function getEntryHandleStatus($entry_id)
    {
        $now   = new DateTime('now');
        $entry = GFAPI::get_entry($entry_id);

        //get expiration datetime
        $expiration_date = $this->getExpirationDate($entry);

        //has proposal been sent before expired date ?
        if ($entry[26] != '') {
            $sentdate = new DateTime($entry[26]);
            if ($expiration_date->format("Y-m-d") > $sentdate->format("Y-m-d")) {
                return ["status" => "handled"];
            }
        }

        //is expired without sent proposal
        if ($expiration_date->format("Y-m-d") < $now->format("Y-m-d")) {
            return ["status" => "expired"];
        } else {
            $current         = $now->settime(0, 0);
            $expiration_date = $expiration_date->settime(0, 0);
            $interval        = $current->diff($expiration_date);

            $int = $interval->format("%r%a");

            return ["status" => "pending", "last" => $expiration_date, "diff" => $int];
        }
    }

    public function getExpirationDate($entry)
    {

        $now             = new DateTime('now');
        $delay           = get_option('tbc_options', array())['delay_lead'];
        $created         = new DateTime($entry['date_created']);
        $expiration_date = $created->modify('+' . $delay . ' day');

        //if manual date is defined
        if ($entry[32] != '') {
            $expiration_date = new DateTime($entry[32]);
        }
        return $expiration_date;
    }

/*
 *
 * Ajax update field values
 */
    public function updateFieldValue()
    {

        if (isset($_POST['securite_nonce']) && isset($_POST['entry_id'])) {
            if (wp_verify_nonce($_POST['securite_nonce'], 'securite-nonce')) {

                $entry = GFAPI::get_entry($_POST['entry_id']);

                switch ($_POST['data_field_id']) {

                        /* case "27":

                if ($_POST['value']==1)
                {
                $entry["27.1"]=0;
                }
                else
                {
                $entry["27.1"]=1;
                }

                GFAPI::update_entry($entry);

                echo $entry["27.1"];

                break; */
                }
            }
        }
    }
/*
 * shortcode userRole
 */
    public function userRole($params, $content = null)
    {
        extract(shortcode_atts(array(
            'role'   => false,
            'except' => false,
        ), $params));

        $current_user = wp_get_current_user();

        if ($role) {
            if (in_array($except, (array) $current_user->roles)) {
                return $content;
            }
        }
        if ($except) {
            if (!in_array($except, (array) $current_user->roles)) {
                return $content;
            }
        }
    }

}
