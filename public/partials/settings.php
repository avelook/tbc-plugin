<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tbc_Settings
{

/**
 * The ID of this plugin.
 *
 * @since    1.0.0
 * @access   private
 * @var      string    $plugin_name    The ID of this plugin.
 */
    private $plugin_name;
/**
 * The version of this plugin.
 *
 * @since    1.0.0
 * @access   private
 * @var      string    $version    The current version of this plugin.
 */
    private $version;
/**
 * Initialize the class and set its properties.
 *
 * @since    1.0.0
 * @param      string    $plugin_name       The name of the plugin.
 * @param      string    $version    The version of this plugin.
 */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;

    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        /* add_options_page(
        'Settings Admin','My Settings','manage_options','my-setting-tbc',array ($this,'create_admin_page')
        ); */

        add_menu_page("TBC", "TBC", 'manage_options', 'my-setting-tbc', array($this, 'create_admin_page'), $icon_url, $position);
    }
    public function create_admin_page()
    {

        // Set class property
        $this->options = get_option('tbc_options');
        ?>

      <div class="wrap">
      <h1>My Settings</h1>
      <form method="post" action="options.php">
      <?php
// This prints out all hidden setting fields
        settings_fields('tbc_option_group');
        do_settings_sections('my-setting-tbc');
        submit_button();
        ?>
              </form>

              <h2>Autre informations</h2>
          <ul>
                      <li>Les 2 pages TBC qui sont ajoutées à la proposition commerciale sont stockées dans le répertoire wp-content/plugins/tbc-plugin/public/pdf/</li>
                      <li>L'url appelée par la tâche cron pour les relances est "team-business-centers.com/wp-json/tbc/v1/recall?pass=16@Opd". Les relances sont envoyées aux adresses mail associées aux centres en backoffice</li>
              </ul>

          </div>
          <?php
}
    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'tbc_option_group', // Option group
            'tbc_options', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'setting_section_id',
            'General', // Title
            array($this, 'print_section_info'), // Callback
            'my-setting-tbc' // Page
        );

        /*    add_settings_field(
        'type_site',
        'Type de site', // Title
        array($this, 'type_site_callback'), // Callback
        'my-setting-tbc', // Page
        'setting_section_id' // Section
        );*/

        /*   add_settings_field(
        'tel',
        'N° de téléphone', // Title
        array($this, 'id_number_callback'), // Callback
        'my-setting-tbc', // Page
        'setting_section_id' // Section
        );*/

        add_settings_field(
            'proposal_cc_address',
            'Adresse mail à laquelle les demandes de contact + contact express, ainsi que les notifications de changement de statut des leads, sont envoyés', // Title
            array($this, 'id_proposal_cc_address'), // Callback
            'my-setting-tbc', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'delay_lead',
            'Durée (en j) de validité d\'un lead (au-delà, le lead est récupéré par TBC et grisé pour le membre)', // Title
            array($this, 'id_delay_lead'), // Callback
            'my-setting-tbc', // Page
            'setting_section_id' // Section
        );
    }
    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        /*  $new_input = array();
        if (isset($input['tel'])) {
        $new_input['tel'] = $input['tel'];
        }*/

        /* if (isset($input['type_site'])) {
        $new_input['type_site'] = $input['type_site'];
        }
         */
        if (isset($input['proposal_cc_address'])) {
            $new_input['proposal_cc_address'] = $input['proposal_cc_address'];
        }

        if (isset($input['delay_lead'])) {
            $new_input['delay_lead'] = $input['delay_lead'];
        }

        return $new_input;
    }
    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print '';
    }
    /**
     * Get the settings option array and print one of its values
     */
    /* public function type_site_callback()
    {
    $options           = get_option('tbc_options');
    $args['label_for'] = "type_site";

    // output the field
    ?>
    <select id="<?php echo esc_attr($args['label_for']); ?>"
    name="tbc_options[<?php echo esc_attr($args['label_for']); ?>]"
    >
    <option value="tbc" <?php echo isset($options[$args['label_for']]) ? (selected($options[$args['label_for']], 'tbc', false)) : (''); ?>>
    <?php esc_html_e('Site TBC principal', 'tbc-plugin');?>
    </option>
    <option value="center" <?php echo isset($options[$args['label_for']]) ? (selected($options[$args['label_for']], 'center', false)) : (''); ?>>
    <?php esc_html_e('Site Centre vitrine', 'tbc-plugin');?>
    </option>
    </select>
    <description>
    <p><em>
    Sélectionnez ici le type de site sur lequel ce plugin a été installé (site TBC ou site centre)
    </em>
    </p>
    </description>

    <?php

    }
     */
    /* public function id_number_callback()
    {
    printf(
    '<input type="text" id="tel" name="tbc_options[tel]" value="%s" />', isset($this->options['tel']) ? esc_attr($this->options['tel']) : ''
    );
    }*/
    public function id_proposal_cc_address()
    {
        printf(
            '<input type="text" id="proposal_cc_address" name="tbc_options[proposal_cc_address]" value="%s" />', isset($this->options['proposal_cc_address']) ? esc_attr($this->options['proposal_cc_address']) : ''
        );
    }
    public function id_delay_lead()
    {
        printf(
            '<input type="text" id="delay_lead" name="tbc_options[delay_lead]" value="%s" />', isset($this->options['delay_lead']) ? esc_attr($this->options['delay_lead']) : ''
        );
    }

}