<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.avelook.fr
 * @since      1.0.0
 *
 * @package    Tbc
 * @subpackage Tbc/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Tbc
 * @subpackage Tbc/public
 * @author     avelook <raphael.thiry@avelook.fr>
 */
class Tbc_Uploader
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        //get absolute path and remove file
        if (!function_exists('get_home_path')) {
            require_once dirname(__FILE__) . '/../../../../../wp-admin/includes/file.php';
        }

        $this->plugin_name     = $plugin_name;
        $this->version         = $version;
        $this->proposalpath    = "/wp-content/uploads/tbc/proposals/";
        $this->attachmentspath = '/wp-content/uploads/tbc/attachments/';
        $this->invoicepath     = '/wp-content/uploads/tbc/invoices/';

        $this->dpath_base        = get_home_path() . $this->proposalpath;
        $this->dstatic_base      = plugin_dir_path(__FILE__) . '/../pdf/';
        $this->uploads_path_base = get_home_path() . $this->attachmentspath;
        $this->invoice_path      = get_home_path() . $this->invoicepath;

        $this->dpath_url        = get_site_url() . $this->proposalpath;
        $this->uploads_path_url = get_site_url() . $this->attachmentspath;
        $this->invoice_path_url = get_site_url() . $this->invoicepath;

        $this->ajax_url = get_site_url() . '/wp-admin/admin-ajax.php';
    }
    /*
     * Upload Proposal and merge with TBC documents
     */
    public function uploadProposal()
    {
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        $current_user = wp_get_current_user();
        $user_id      = $current_user->ID;

        //variables
        $entry_id = $_POST['entry_id'];

        if (!function_exists('get_home_path')) {
            require_once dirname(__FILE__) . '/../../wp-admin/includes/file.php';
        }

        if (isset($_POST['securite_nonce']) && isset($_POST['entry_id']) && $user_id >= 1) {
            if (wp_verify_nonce($_POST['securite_nonce'], 'securite-nonce')) {

                //upload
                $options = array('upload_dir' => $this->dpath_base . $entry_id . '/',
                    'upload_url'                  => $this->dpath_url . '/' . $entry_id . '/');

//rename file
                $_FILES["files"]["name"] = str_replace(["'", 'é', ' '], ['', 'e', ''], $_FILES["files"]["name"]);

                //upload proposal
                ob_start();
                $upload_handler = new UploadHandler($options);
                $json           = ob_get_contents();
                ob_clean();
                $response = $upload_handler->response;
                $files    = $response['files'];

                $file_count    = count($files);
                $filename      = $files[0]->name;
                $complete_path = $this->dpath_base . $entry_id . '/' . $filename;
                $complete_url  = $this->dpath_url . $entry_id . '/' . $filename;

                //check if orientation is right
                if ($this->checkOrientation($complete_path) == false) {
                    //Remove file
                    unlink($complete_path);
                    return ["error" => true, "msg" => __("Votre proposition contient des pages au format paysage. Elle n'a donc pas pu être uploadée. Merci de corriger le format de votre proposition", "tbc_plugin")];
                }

                //merge pdfs with first and last page
                if (!empty($this->uploadMergePdf($complete_path))) {
                    return ["error" => true, "msg" => 'Une erreur est survenue'];
                }

                //update Gravity Field
                $editentry = GFAPI::get_entry($_POST['entry_id']);
                foreach (json_decode($json) as $file) {
                    $result = $file[0]->url;
                }
                $editentry[29] = $result;

                ob_start();
                $updated = GFAPI::update_entry($editentry);
                $updated = ob_get_contents();
                ob_clean();
                if (empty($updated)) {
                    //   $return=["error"=>false,"result"=>$complete_url,"msg"=>json_decode($json)];
                    //  echo json_encode($return);
                    wp_send_json(["error" => false, "result" => $complete_url, "msg" => json_decode($json)]);
                    //   wp_send_json($json);
                } else {
                    wp_send_json(["error" => true, "msg" => 'Une erreur est survenue au moment de l\'enregistrement en bdd']);
                }
            }
        }

        //  die();
    }
    /*
     * check page layout orientation
     */
    public function checkOrientation($file_path)
    {
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        //$pdf=new \fpdi\FPDI;//old plugin
        $pdf = new \setasign\Fpdi\Fpdi;

        $pageCount = $pdf->setSourceFile($file_path);
        for ($i = 1; $i <= $pageCount; $i++) {
            $template = $pdf->importPage($i);
            $size     = $pdf->getTemplateSize($template);

            $orientation = $size['orientation'];
            //  $width    =$pdf->CurOrientation;
            if ($orientation == "L") {
                return false;
            }
        }
        return true;
    }
    /*
     * merge pdf with TBC pages
     */
    public function uploadMergePdf($complete_path)
    {

        $pdf = new \setasign\Fpdi\Fpdi;

        $files = array(
            $this->dstatic_base . 'tbc-first-page.pdf',
            $complete_path,
            $this->dstatic_base . 'tbc-last-page.pdf',
        );
        foreach ($files as $file) {
// get the page count

            $pageCount = $pdf->setSourceFile($file);
// iterate through all pages

            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
// import a page
                $templateId = $pdf->importPage($pageNo);
// get the size of the imported page
                $size = $pdf->getTemplateSize($templateId);

// add a page with the same orientation and size
                $pdf->AddPage($size['orientation'], $size);

// use the imported page
                $pdf->useTemplate($templateId);

                //$pdf->SetFont('Helvetica');
                //  $pdf->SetXY(5,5);
            }
        }
        return $pdf->Output($complete_path, "F");
    }
    /*
     * Send proposal
     */
    public function sendProposal()
    {

        if (isset($_POST['securite_nonce'])) {
            if (wp_verify_nonce($_POST['securite_nonce'], 'securite-nonce')) {

                $entry_id   = $_POST['entry_id'];
                $from_name  = $_POST['from_email'];
                $from_email = $_POST['from_email'];
                $subject    = $_POST['subject'];
                $message    = stripslashes($_POST['message']);

                $message           = str_replace("wp-content", site_url() . "/wp-content", $message);
                $cc                = $_POST['cc'];
                $cc                = explode(",", $cc);
                $attachment_name   = $_POST['attachment_name'];
                $attachments_files = $_POST['uploadedfiles'];

                //entry get recipient
                $entry     = GFAPI::get_entry($entry_id);
                $recipient = $entry[3];

                $commercial = get_option('tbc_options', array())['proposal_cc_address'];

                //attachments (proposal + uploaded files)
                $attachments = [];

                $attachments[] = $this->dpath_base . $entry_id . '/' . $attachment_name;

                if ($attachments_files) {
                    foreach ($attachments_files as $file) {
                        $attachments[] = $this->uploads_path_base . $entry_id . '/' . $file;
                    }
                }

                //prepare sending
                $to        = $recipient;
                $headers   = array('Content-Type: text/html; charset=UTF-8');
                $headers[] = 'From: Team Business Centers <' . $from_email . '>';

                //cc
                $cc[] = $commercial;

                foreach ($cc as $c) {
                    if ($c != '') {
                        $headers[] = 'Cc: ' . $c . '';
                    }
                }

                $mail = wp_mail($to, $subject, $message, $headers, $attachments);

                //after sending mail, we update the entry
                $now       = date('Y-m-d H:i:s');
                $entry[26] = $now;
                $entry[13] = 2; //status
                GFAPI::update_entry($entry);

                //send notifications after updating entry
                Tbc_GForms::NotificationAfterUpdateEntry($entry_id);
            }
        }
    }
    /*
     * remove a proposal entry file
     */
    public function removeProposal()
    {
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        $current_user = wp_get_current_user();
        $user_id      = $current_user->ID;

        if ($user_id < 1) {

            die();
        }

        if (isset($_POST['securite_nonce']) && isset($_POST['entry_id'])) {
            if (wp_verify_nonce($_POST['securite_nonce'], 'securite-nonce')) {

                /*

                $relative_path = $editentry[29];

                //get absolute path and remove file
                if (!function_exists('get_home_path')) {
                require_once dirname(__FILE__) . '/../../../../../wp-admin/includes/file.php';
                }

                $complete_path = str_replace(get_site_url(), get_home_path(), $relative_path);
                $complete_path = $this->_cleanfilename(str_replace("//", "/", $complete_path)); //fix

                var_dump($relative_path);
                var_dump(file_exists($complete_path));
                 */

                $filename      = $_POST['filename'];
                $complete_path = $this->dpath_base . '/' . $_POST['entry_id'] . '/' . $filename;

                if (file_exists($complete_path)) {
                    unlink($complete_path);

                    //update Gravity Field
                    $editentry     = GFAPI::get_entry($_POST['entry_id']);
                    $editentry[29] = "";
                    $updated       = GFAPI::update_entry($editentry);

                    echo json_encode(1);
                } else {
                    return false;
                }
            }
        }

        die();
    }
    /*
     * AJAX Upload and update database field entry
     */
    public function uploadFile()
    {
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        if (wp_verify_nonce($_POST['securite_nonce'], 'securite-nonce')) {
            $entry_id = $_POST['entry_id'];
            $field_id = $_POST['field_id'];

            switch ($field_id) {
                //single upload file (invoice)
                case 25:
                    $options = array(
                        'upload_dir' => $this->invoice_path . '/' . $entry_id . '/',
                        'upload_url' => $this->invoice_path_url . '/' . $entry_id . '/',
                        'script_url' => $this->ajax_url . '?entry_id=' . $entry_id);

                    break;

                //multiple upload file (json string)
                case 31:
                    $options = array(
                        'upload_dir' => $this->uploads_path_base . '/' . $entry_id . '/',
                        'upload_url' => $this->uploads_path_url . '/' . $entry_id . '/',
                        'script_url' => $this->ajax_url . '?entry_id=' . $entry_id);

                    break;

            }

            //upload
            ob_start();
            $upload_handler = new UploadHandler($options);
            $json           = ob_get_contents();
            ob_clean();

            //prepare update field with json
            $editentry = GFAPI::get_entry($entry_id);
            $files     = [];

            switch ($field_id) {

                //single upload file (invoice)
                case 25:
                    $files                = json_decode($json);
                    $editentry[$field_id] = $files->files[0]->url;
                    /*
                    //change status => 5=invoice
                    $editentry[13] = 5;*/

                    break;

                //multiple upload file (json string)
                case 31:
                    //get current value and add new value
                    $value = json_decode($editentry[$field_id]);

                    if ($value != '') {
                        foreach ($value as $file) {
                            $files[] = $file;
                        }
                    }
                    foreach (json_decode($json) as $file) {
                        $files[] = $file[0];
                    }
                    //update
                    $editentry[$field_id] = json_encode($files);
                    break;
            }

            ob_start();
            $updated = GFAPI::update_entry($editentry);
            $updated = ob_get_contents();
            ob_clean();

            wp_send_json($json);
        }
    }
    /*
     * Delete file of an entry field and update database field entry
     */
    public function deleteFile()
    {
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        if (wp_verify_nonce($_POST['securite_nonce'], 'securite-nonce')) {
            $field_id = $_POST['field_id'];
            $pos      = $_POST['pos'];
            $entry_id = $_POST['entry_id'];
            $file     = $_GET['file'];

            switch ($field_id) {

                //single upload file (text string)
                case 25:
                    $editentry            = GFAPI::get_entry($entry_id);
                    $editentry[$field_id] = '';

                    //delete file
                    $filepath = $this->invoice_path . '/' . $entry_id . '/' . $file;

                    break;

                //multiple upload file (json string)
                case 31:
                    $editentry = GFAPI::get_entry($entry_id);
                    $files     = json_decode($editentry[$field_id]);
                    $narray    = [];
                    foreach ($files as $key => $file) {
                        if ($key != $pos) {
                            $narray[] = $file;
                        }
                    }
                    if (empty($narray)) {
                        $nval = '';
                    }

                    $editentry[$field_id] = json_encode($narray);

                    //delete file

                    $filepath = $this->uploads_path_base . '/' . $entry_id . '/' . $file->name;

                    break;
            }

            if ($filepath) {
                wp_delete_file($filepath);
                $updated = GFAPI::update_entry($editentry);
            }

        }
    }

    public function addInlineScript()
    {

    }

    protected function _cleanfilename($str)
    {

        $sep    = '_';
        $strict = false;
        $trim   = 248;

        $str = strip_tags(htmlspecialchars_decode(strtolower($str))); // lowercase -> decode -> strip tags
        $str = str_replace("%20", ' ', $str); // convert rogue %20s into spaces
        $str = preg_replace("/%[a-z0-9]{1,2}/i", '', $str); // remove hexy things
        $str = str_replace("&nbsp;", ' ', $str); // convert all nbsp into space
        $str = preg_replace("/&#?[a-z0-9]{2,8};/i", '', $str); // remove the other non-tag things
        $str = preg_replace("/\s+/", $sep, $str); // filter multiple spaces
        $str = preg_replace("/\.+/", '.', $str); // filter multiple periods
        $str = preg_replace("/^\.+/", '', $str); // trim leading period

        if ($strict) {
            $str = preg_replace("/([^\w\d\\" . $sep . ".])/", '', $str); // only allow words and digits
        } else {
            $str = preg_replace("/([^\w\d\\" . $sep . "\[\]\(\).])/", '', $str); // allow words, digits, [], and ()
        }

        $str = preg_replace("/\\" . $sep . "+/", $sep, $str); // filter multiple separators
        $str = substr($str, 0, $trim); // trim filename to desired length, note 255 char limit on windows

        return $str;

    }

}
