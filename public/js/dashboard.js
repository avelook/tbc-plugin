/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery( document ).ready( function () {

//autocomplete
 jQuery( "#dashbselect-centers" ).combobox( {
   select: function (event, ui) {
var values = jQuery( this ).parents( "form" ).serializeArray();
	getStats( values );

    }
 	});


//init
    var values = jQuery( "#dashboardForm" ).serializeArray();
    getStats( values );
    jQuery( "#dashboardForm select" ).on( "change", function ()
    {
	var values = jQuery( this ).parents( "form" ).serializeArray();
	getStats( values );
    } );






// ==============================================================
// This is for the animation
// ==============================================================
    var chart = [ ];

    for ( var i = 0; i < chart.length; i++ ) {
	chart[i].on( 'draw', function ( data ) {
	    if ( data.type === 'line' || data.type === 'area' ) {
		data.element.animate( {
		    d: {
			begin: 500 * data.index,
			dur: 500,
			from: data.path.clone().scale( 1, 0 ).translate( 0, data.chartRect.height() ).stringify(),
			to: data.path.clone().stringify(),
			easing: Chartist.Svg.Easing.easeInOutElastic
		    }
		} );
	    }
	    if ( data.type === 'bar' ) {
		data.element.animate( {
		    y2: {
			dur: 500,
			from: data.y1,
			to: data.y2,
			easing: Chartist.Svg.Easing.easeInOutElastic
		    },
		    opacity: {
			dur: 500,
			from: 0,
			to: 1,
			easing: Chartist.Svg.Easing.easeInOutElastic
		    }
		} );
	    }

	} );
    }


} );


/*
 *
 * Return stats depending values of search form
 */
function getStats( values )
{
    var action = 'getStats';
    var object = { 'values': values };
    var callback = function ( array ) {

	var states = array.states;
	var colors = [ '#f7951e', '#745af2', '#26c6da', '#1e88e5', '#109a68', '#39e85b', '#eceff1' ];
	renderChart( 'Statuts', 'donut', '.ct-chart-states', states, colors );

	var types = array.types;
	var colors = [ '#f7951e', '#745af2', '#26c6da', '#1e88e5', '#109a68', '#39e85b', '#eceff1' ];
	renderChart( 'Types', 'donut', '.ct-chart-types', types, colors );//donut

	var types = array.types;
	renderTable( '.ct-chart-tabletypes', 'percent', types );



    };

    getAjaxData( action, object, callback );
}




/*
 * Generic ajax request function
 */
function getAjaxData( action, object, callback )
{

    var wpdata = {
	'action': action,
	'securite_nonce': tbc_vars.securite_nonce
    };

    var data = Object.assign( object, wpdata );
    var selector = jQuery( ".ct-chart" );
    var tableselector= jQuery(".ct-chart-tabletypes");

    jQuery.ajax(
	{
	    type: "GET",
	    url: tbc_vars.ajaxurl,
	    data: data,
	    dataType: "json",
	    cache: false,
	    beforeSend: function ()
	    {
		jQuery( ".widget-loader" ).remove();
		/*selector.html( "" );*/
		jQuery(".ct-chart-wrapper").prepend( "<div class='widget-loader'>chargement...</div>" );
	    },
	    success: function ( msg )
	    {
		jQuery( ".widget-loader" ).remove();
		response = msg;
		//console.log( msg );
		//response = JSON.stringify( msg );
		return callback( response );

	    },
	    error: function ( data )
	    {
		jQuery( ".widget-loader" ).remove();
		selector.html( "<div class='widget-loader'>Pas de résultat</div>" );
		tableselector.html("");
	    }
	} );


}


/*
 * Render Chart
 */

function renderChart( title, type, selector, array, colors )
{

    var chart = c3.generate( {
	bindto: selector,
	data: {
	    columns: array,
	    type: type,
	    /*  onclick: function ( d, i ) {
	     console.log( "onclick", d, i );
	     },
	     onmouseover: function ( d, i ) {
	     console.log( "onmouseover", d, i );
	     },
	     onmouseout: function ( d, i ) {
	     console.log( "onmouseout", d, i );
	     }*/
	},
	donut: {
	    label: {
		show: false
	    },
	    title: title,
	    width: 20,
	},
	/*plugins: [
	 Chartist.plugins.legend( {
	 legendNames: [ 'Blue pill', 'Red pill', 'Purple pill' ],
	 } )
	 ],*/
	legend: {
	    /*    name: [ 'Blue pill', 'Red pill', 'Purple pill' ],*/
	    //    hide: true
	    //or hide: 'data1'
	    //or hide: ['data1', 'data2']
	},
	color: {
	    pattern: colors
	}
    } );


    console.log( chart );
    /* chart.plugins.legend( {
     legendNames: [ 'Blue pill', 'Red pill', 'Purple pill' ],
     } )*/


}



/*
 * Function that render table
 */
function renderTable( selector, type, values )
{

    console.log( selector );
    console.log( "values" );
    console.log( values );

    switch ( type )
    {

	case   'percent':

	    var html = '<table class="table table-striped">';
	    // html += '<tr> <th>Description</th><th>%</th></tr>';

	    for ( var i = 0; i < values.length; i++ ) {

		html += '<tr> <td>' + values[i][0] + '</td><td>' + Math.round( values[i][1] ) + ' %</td></tr>';
	    }
	    html += '</table>';

	    jQuery( selector ).html( html );
	    break;


    }



}
