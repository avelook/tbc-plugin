jQuery( function ( ) {

    $ = jQuery;



    /*
     * TRIGGER WHEN STATUS IS CHANGING (GRAVITY VIEW)
     */

    jQuery( document ).on( 'focus', ".entity_status", function () {
	// Store the current value on focus and on change
	previous = this.value;
    } ).on( "change", ".entity_status", function ( )
    {
	var entry_id = $( 'option:selected', this ).attr( "entry_id" );
	selector=$(this);
	var value = selector.val();


	//if status is "refused" show modal
	if ( value == 6 )
	{
	    var cl = $( this );
	    var state = $( this ).val();
	    var textarea = "<textarea style='width:100%'></textarea><div class='controls'><a class='btn btn-primary m-submit'>Valider</a><a class='btn m-cancel btn-light'>Annuler</a></div>";
	    var dmodal = $( "#DynamicModal" );
	    dmodal.find( ".modal-header h4" ).html( "Pourquoi cette demande a-t-elle été refusée ?" );
	    dmodal.find( ".modal-body" ).html( textarea );
	    $( '#DynamicModal' ).modal( { show: true } );

	    $( ".m-submit" ).on( "click", function (  )
	    {
		var content = $( "#DynamicModal" ).find( ".modal-body textarea" ).val();
		data = { 'fields': [ { 'id': 13, 'value': state }, { 'id': 30, 'value': content } ], 'action': 'updateentrystatus', 'entry_id': entry_id };
		updateStatus(selector, data );
	    } );

	    $( ".m-cancel" ).on( "click", function ()
	    {
		//   console.log( previous );
		cl.val( previous );
	    } );

	    $( "#DynamicModal" ).on( "hide.bs.modal", function ()
	    {
	    } );

	    return false;
	} else
	{
	    if ( confirm( 'Etes-vous sûr de vouloir modifier le statut de cette demande ?' ) ) {

		data = { 'fields': [ { 'id': 13, 'value': $( this ).val( ) } ], 'action': 'updateentrystatus', 'entry_id': entry_id };
		
		updateStatus( selector,data );

	    } else
	    {
		//   console.log( previous );
		jQuery( this ).val( previous );
		return false;
	    }
	}


    } )


    /*update status field*/
    function updateStatus( selector,data )
    {

	//update status
	var beforesend = function (  ) {
	    selector.after( "<div class='status-preloader'><img src='" + tbc_vars.base_url + "/wp-content/plugins/tbc-plugin/public/images/preloader.gif'></div>" );
	    selector.hide( );
	    if ( $( '#DynamicModal' ).css( "display" ) == "block" )
	    {
		$( '#DynamicModal' ).modal( 'hide' );
	    }
	};
	var callback = function ( msg, selector ) {
	    $( ".status-preloader" ).remove();
	    $(".entity_status").show( );


	};
	ajaxUpdate( data, beforesend, callback );
    }



    jQuery( ".updated_entry_field" ).on( "click", function () {

	var entry_id = jQuery( this ).attr( "data-entry-slug" );
	var data_field_id = jQuery( this ).attr( "data-field-id" );
	var value = jQuery( this ).attr( "data-value" );
	var securite_nonce = tbc_vars.securite_nonce;

	var data = {
	    'action': 'updatefieldvalue',
	    'securite_nonce': securite_nonce,
	    'entry_id': entry_id,
	    'data_field_id': data_field_id,
	    'value': value };

	jQuery.ajax(
	    {
		type: "POST",
		context: this,
		url: tbc_vars.ajaxurl,
		data: data,
		dataType: "json",
		cache: false,
		beforeSend: function ( )
		{
		    jQuery( this ).after( "<p><img src='" + tbc_vars.base_url + "/wp-content/plugins/tbc-plugin/public/images/preloader.gif'></p>" );
		    jQuery( this ).remove( );
		},
		success: function ( msg )
		{
		    //    location.reload();
		    alert( "ok" );
		    console.log( this );


		}
	    } );


    } );



    /* -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * PROPOSAL POPUP
     -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------   */


    /*
     * VALIDATE FORM WITH PROPOSAL PDF
     */
    jQuery( "#proposalForm" ).validate(
	{
	    ignore: [ ],
	    messages: {
		attachment_name: {
		    required: "Vous devez d'abord uploader votre proposition commerciale"
		},
		message: {
		    required: "Ce champ est obligatoire"
		},
		subject:
		    {
			required: "Vous devez renseigner un sujet"
		    }
	    }

	}

    );

    /*
     * SEND PROPOSAL
     */
    jQuery( "#proposalForm" ).on( "submit", function ( event )
    {
	if ( jQuery( "#proposalForm" ).valid( ) )
	{
	    tinyMCE.triggerSave();
	    var url = $( this ).attr( "url" );
	    var data = $( this ).serialize( );
	    jQuery.ajax(
		{
		    type: "POST",
		    context: this,
		    url: url,
		    data: data,
		    dataType: "json",
		    cache: false,
		    beforeSend: function ( )
		    {
			jQuery( ".modal-body form" ).hide();
			jQuery( ".modal-body" ).prepend( "<p>" + tbc_vars.sending_proposal.sending_process + "</p>" );

		    },
		    success: function ( msg )
		    {
			jQuery( ".modal-title" ).html( tbc_vars.sending_proposal.success_heading );
			jQuery( ".modal-body" ).html( tbc_vars.sending_proposal.success_message );
			setTimeout( function ( ) {
			    location.reload( );
			}, 3000 );
			response = JSON.stringify( msg );
		    }
		} );
	}
	event.preventDefault( );
    } );

    /*
     * INIT TINYMCE
     */

    tinyMCE.init( { selector: '.tinymce',
	relative_urls: false,
	setup: function ( editor ) {
	    editor.on( 'change', function () {
		editor.save();
	    } );
	}
    } );






    /*
     *
     * AJAX UPLOAD PROPOSAL
     */

    /*file upload*/
    $( '#proposalupload' ).fileupload( {
	singleFileUploads: true,
	add: function ( e, data ) {
	    //console.log( data.originalFiles[0] );
	    var input = $( e.target ).parents( ".form-group" ).find( ".btn-file" );
	    input.removeClass( 'error' );
	    jQuery( ".valid_errmsg" ).remove( );
	    var uploadErrors = [ ];
	    if ( data.originalFiles[0]['type'].length && data.originalFiles[0]['type'] !== "application/pdf" ) {
		uploadErrors.push( 'Vous ne pouvez uploader que des pdf !' );
	    }
	    if ( data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000 ) {
		uploadErrors.push( 'Votre fichier est trop lourd. Merci d\'uploader un fichier inférieur à 5Mo' );
	    }
	    if ( uploadErrors.length > 0 ) {
		input.addClass( 'error' );
		input.after( "<p class='valid_errmsg'>" + uploadErrors.join( "\n" ) + "</p>" );
	    } else {
		data.submit( );
	    }
	},
	progressall: function ( e, data ) {
	    jQuery( "#proposal .files" ).removeClass( "error" ).html( "" );
	    var progress = parseInt( data.loaded / data.total * 100, 10 );
	    $( '#proposal  .progress .bar' ).css(
		'width',
		progress + '%'
		);
	},
	done: function ( e, data ) {

	    $( '#proposal  .progress .bar' ).css(
		'width', '0%'
		);

	    var data = data.result;


	    if ( data.error === true )
	    {
		jQuery( "#proposal  .files" ).addClass( "error" ).html( data.msg );

	    } else
	    {

		jQuery.each( data.msg.files, function ( index, file ) {

		    var url = tbc_vars.puploads + file.name;
		    jQuery( "#proposal  .files" ).html( "Votre proposition : <a href='" + url + "'  target='_blank'>" + file.name + "</a><button class='delete remove-proposal '><i class='fa fa-times'></i></button>" );
		    jQuery( "input[name='attachment_name']" ).val( file.name );
		    jQuery( "input[name='files']" ).val( url );
		} );
	    }
	}
    } ).bind( 'fileuploadsubmit', function ( e, data ) {
// The example input, doesn't have to be part of the upload form:
	data.formData = {
	    securite_nonce: jQuery( "input[name='securite_nonce']" ).val( ),
	    action: 'uploadProposal',
	    entry_id: jQuery( "input[name='entry_id']" ).val( )
	}
    } );


    $( document ).on( "click", ".remove-proposal", function ()
    {
	if ( confirm( 'Etes-vous sûr de vouloir supprimer votre proposition ?' ) ) {

	    var entry_id = $( "#proposalForm input[name='entry_id']" ).val();

	    var data = {
		'action': 'removeproposal',
		'securite_nonce': tbc_vars.securite_nonce,
		'filename':jQuery("input[name='attachment_name']").val(),
		'entry_id': entry_id };
	    jQuery.ajax(
		{
		    type: "POST",
		    context: this,
		    url: tbc_vars.ajaxurl,
		    data: data,
		    dataType: "json",
		    cache: false,
		    beforeSend: function ()
		    {
			var preloader_url = tbc_vars.tbc_plugin_public + 'images/preloader.gif';
			$( "#proposal_preview" ).html( "<span><img src=" + preloader_url + " width='25'/></span>" );
		    },
		    success: function ( msg )
		    {
			$( "#proposal .files" ).html( "" );
			$( "#proposal input[name='attachment_name']" ).val( '' );
			$( "#proposal input[name='files']" ).val( '' );
		    }
		} );

	preventDefault();

	}
    } );




    /*
     *
     * AJAX UPLOAD SIMPLE
     */

    /*file upload*/

    $( '.simpleajaxupload' ).fileupload( {
	singleFileUploads: true,
	limitMultiFileUploads: 1,
	maxNumberOfFiles: 1,
	autoUpload: true,
	add: function ( e, data ) {
	    var uploadErrors = [ ];
	    if ( data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000 ) {
		uploadErrors.push( 'Votre fichier est trop lourd. Merci d\'uploader un fichier inférieur à 5Mo' );
	    }
	    if ( uploadErrors.length > 0 ) {
		input.addClass( 'error' );
		input.after( "<p class='valid_errmsg'>" + uploadErrors.join( "\n" ) + "</p>" );
	    } else {
		data.submit( );
	    }
	},
	progressall: function ( e, data ) {

	    var progress = parseInt( data.loaded / data.total * 100, 10 );
	    $( e.target ).parents( ".form-group" ).find( ".bar" ).css(
		'width',
		progress + '%'
		);
	},
	done: function ( e, data ) {
	    $( e.target ).parents( ".form-group" ).find( ".bar" ).css(
		'width', '0%'
		);

	    if ( data.error === true )
	    {
		$( e.target ).addClass( "error" ).html( data.msg );

	    } else
	    {
		var data = JSON.parse( data.result );
		$.each( data.files, function ( index, file ) {
		    var url = file.url;
		    //Get pos
		    var containerfiles = $( e.target ).parents( ".form-group" ).find( ".files" );
		    var pos = containerfiles.find( "button" ).length;
		    var field_id = $( e.target ).attr( "field_id" );

		    containerfiles.html( '' );
		    containerfiles.append( "<a href='" + url + "'  target='_blank'>" + file.name + "</a><button href='#' class='delete'  action='deleteFile' pos='" + pos + "' field_id='" + field_id + "' data-type='" + file.deleteType + "' data-url='" + file.deleteUrl + "' title='Delete'>\n\
     <input type='hidden' name='uploadedfiles[]' value='" + file.name + "'>\n\
     <i class='fa fa-times'></i></button>" );

		} );


	    }
	}
    } ).bind( 'fileuploadsubmit', function ( e, data ) {
	// The example input, doesn't have to be part of the upload form:
	console.log( "issingle" );
	link = data.fileInput;
	issingle = data.fileInput.hasClass( "single-upload" );
	console.log( issingle );

	var field_id = data.fileInput.attr( 'field_id' );
	var action = data.fileInput.attr( 'action' );

	data.formData = {
	    securite_nonce: tbc_vars.securite_nonce,
	    field_id: field_id,
	    action: action,
	    entry_id: tbc_vars.entry_id
	};
    } );





    /*
     *
     * AJAX UPLOAD MULTIPLE ATTACHMENT FILES
     */

    $( '.ajaxupload' ).fileupload( {
	singleFileUploads: true,
	limitMultiFileUploads: 1,
	maxNumberOfFiles: 1,
	autoUpload: true,
	add: function ( e, data ) {
	    var uploadErrors = [ ];
	    if ( data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000 ) {
		uploadErrors.push( 'Votre fichier est trop lourd. Merci d\'uploader un fichier inférieur à 5Mo' );
	    }
	    if ( uploadErrors.length > 0 ) {
		input.addClass( 'error' );
		input.after( "<p class='valid_errmsg'>" + uploadErrors.join( "\n" ) + "</p>" );
	    } else {
		data.submit( );
	    }
	},
	progressall: function ( e, data ) {

	    var progress = parseInt( data.loaded / data.total * 100, 10 );
	    $( e.target ).parents( ".form-group" ).find( ".bar" ).css(
		'width',
		progress + '%'
		);
	},
	done: function ( e, data ) {
	    $( e.target ).parents( ".form-group" ).find( ".bar" ).css(
		'width', '0%'
		);

	    if ( data.error === true )
	    {
		$( e.target ).addClass( "error" ).html( data.msg );

	    } else
	    {
		var files = [ ];

		var data = JSON.parse( data.result );
		console.log( "data" );
		console.log( data.files );

		$.each( data.files, function ( index, file ) {

		    var url = file.url;
		    var containerfiles = $( e.target ).parents( ".form-group" ).find( ".files" );
		    var pos = containerfiles.find( "button" ).length;
		    var field_id = $( e.target ).attr( "field_id" );

		    containerfiles.append( "<a href='" + url + "'  target='_blank'>" + file.name + "</a><button href='#' class='delete'  action='deleteFile' pos='" + pos + "' field_id='" + field_id + "' data-type='" + file.deleteType + "' data-url='" + file.deleteUrl + "' title='Delete'>\n\
		<input type='hidden' name='uploadedfiles[]' value='" + file.name + "'>\n\
		<i class='fa fa-times'></i></button>" );

		    files.push( file );
		} );


	    }
	}
    } ).bind( 'fileuploadsubmit', function ( e, data ) {
// The example input, doesn't have to be part of the upload form:
	console.log( "issingle" );
	link = data.fileInput;
	issingle = data.fileInput.hasClass( "single-upload" );
	console.log( issingle );

	var field_id = data.fileInput.attr( 'field_id' );
	var action = data.fileInput.attr( 'action' );

	data.formData = {
	    securite_nonce: tbc_vars.securite_nonce,
	    field_id: field_id,
	    action: action,
	    entry_id: tbc_vars.entry_id
	};
    } );




    /*
     *
     * DELETE FILE AJAX
     */
    $( document ).on( 'click', '.delete', function ( e ) {
	e.preventDefault();
	var $link = $( this );

	//preloader
	$link.html( "<img width=15 src='" + tbc_vars.base_url + "/wp-content/plugins/tbc-plugin/public/images/preloader.gif'>" );
	var container = $link.parents( ".files" );

	var req = $.ajax( {
	    dataType: 'json',
	    context: this,
	    data: { 'action': $link.attr( 'action' ),
		'field_id': $link.attr( "field_id" ),
		'entry_id': tbc_vars.entry_id,
		'pos': $link.attr( "pos" ),
		'securite_nonce': tbc_vars.securite_nonce },
	    url: $link.data( 'url' ),
	    //  url: tbc_vars.ajaxurl,
	    type: 'POST'
	} ).done( function ( e, res ) {
	    $( this ).prev().remove();
	    $( this ).remove();
	} );
    } );



} );




/*
 * helper
 */
var tbc_helper =
    {
	showStickyMsg: function ( msg, type )
	{

	    jQuery( "#stickymsgbox > div" ).html( msg );
	    jQuery( "#stickymsgbox > div" ).attr( "class", "alert-" + type );
	    jQuery( "#stickymsgbox" ).show();

	    setTimeout( function ( ) {
		$( '#stickymsgbox' ).hide();
	    }, 5000 );
	}
    };




/*
 * GENERIC FUNCTIONS
 */

/*
 * UPDATE AJAX FUNCTION
 */
function ajaxUpdate( data, beforesend, callback )
{

    jQuery.ajax(
	{
	    type: "POST",
	    context: this,
	    url: tbc_vars.ajaxurl,
	    data: data,
	    dataType: "json",
	    cache: false,
	    beforeSend: function ( )
	    {
		beforesend();
	    },
	    success: function ( msg )
	    {
		callback( msg );

	    },
	    error: function ( msg )
	    {
		alert( "Une erreur est survenue. Merci de prendre contact avec les administrateurs du site" );
	    }

	} );
}


/*COMBOBOX*/

jQuery( function ()
{

    $( "#search-box-filter_23" ).wrap( " <div class='ui-widget'> </div>" );
    /*
     <div class="ui-widget">
     <label>Your preferred programming language: </label>
     <select id="combobox">
     <option value="">Select one...</option>
     <option value="ActionScript">ActionScript</option>
     <option value="AppleScript">AppleScript</option>
     <option value="Asp">Asp</option>
     <option value="BASIC">BASIC</option>
     <option value="C">C</option>
     <option value="C++">C++</option>
     <option value="Clojure">Clojure</option>
     <option value="COBOL">COBOL</option>
     <option value="ColdFusion">ColdFusion</option>
     <option value="Erlang">Erlang</option>
     <option value="Fortran">Fortran</option>
     <option value="Groovy">Groovy</option>
     <option value="Haskell">Haskell</option>
     <option value="Java">Java</option>
     <option value="JavaScript">JavaScript</option>
     <option value="Lisp">Lisp</option>
     <option value="Perl">Perl</option>
     <option value="PHP">PHP</option>
     <option value="Python">Python</option>
     <option value="Ruby">Ruby</option>
     <option value="Scala">Scala</option>
     <option value="Scheme">Scheme</option>
     </select>
     </div>
     <button id="toggle">Show underlying select</button>
     */

    jQuery( "#search-box-filter_23" ).combobox();


    jQuery( "#search-box-filter_23" ).on( "autocompletechange", function (  ) {
	alert( "changed" );
	jQuery( ".gv-widget-search" ).submit();

    } );



} );