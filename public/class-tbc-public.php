<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.avelook.fr
 * @since      1.0.0
 *
 * @package    Tbc
 * @subpackage Tbc/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Tbc
 * @subpackage Tbc/public
 * @author     avelook <raphael.thiry@avelook.fr>
 */
class Tbc_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version     = $version;
        //define('PLUGIN_TBC_PATH',plugin_dir_path(__FILE__));
    }
    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Tbc_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Tbc_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/tbc-public.css', array(), $this->version, 'all');
        wp_enqueue_style('tbc-styles', plugin_dir_url(__FILE__) . 'css/styles.css', false);
    }
    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Tbc_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Tbc_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        /*    wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/tbc-public.js', array('jquery'), $this->version, false);
    wp_enqueue_script('jquery-validate', 'https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js', array('jquery'), 1.0, true);
    wp_enqueue_script('ui-widget', plugin_dir_url(__FILE__) . 'js/jquery.ui.widget.js', array('jquery'));
    //wp_enqueue_script('ui-widget','https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js',array ('ui-widget'));
    wp_enqueue_script('iframe-transport', plugin_dir_url(__FILE__) . 'js/jquery.iframe-transport.js', array('jquery', 'ui-widget'));
    wp_enqueue_script('jquery-file-upload', plugin_dir_url(__FILE__) . 'js/jquery.fileupload.js', array('jquery', 'ui-widget', 'iframe-transport'));
    //wp_enqueue_script('tinymce','https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.13/jquery.tinymce.min.js',array (),1.0,false);

    wp_enqueue_script('juicombobox', plugin_dir_url(__FILE__) . 'js/jquery.ui-combobox.js');
    wp_enqueue_script('front-js', plugin_dir_url(__FILE__) . 'js/front.js', array('jquery', 'ui-widget', 'iframe-transport', 'jquery-file-upload', 'tinymce'));

    wp_enqueue_script('dashboard-js', plugin_dir_url(__FILE__) . 'js/dashboard.js', array('chartist-js'));
    //wp_enqueue_script('dialogfx-js',plugin_dir_url(__FILE__).'js/dialogFx.js',array ('jquery','ui-widget'));
     */
    }

}
