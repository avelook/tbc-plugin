<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.avelook.fr
 * @since      1.0.0
 *
 * @package    Tbc
 * @subpackage Tbc/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Tbc
 * @subpackage Tbc/includes
 * @author     avelook <raphael.thiry@avelook.fr>
 */
class Tbc
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Tbc_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        if (defined('PLUGIN_NAME_VERSION')) {
            $this->version = PLUGIN_NAME_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'tbc-plugin';

        require dirname(__FILE__) . '/../vendor/autoload.php';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_global_hooks();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Tbc_Loader. Orchestrates the hooks of the plugin.
     * - Tbc_i18n. Defines internationalization functionality.
     * - Tbc_Admin. Defines all hooks for the admin area.
     * - Tbc_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-tbc-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-tbc-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-tbc-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-tbc-public.php';

        /*
         * Load UploadHandler Class
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/uploadHandler.php';

        /**
         * Various
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/gforms-functions.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/gviews-functions.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/toolset-functions.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/helper.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/uploader.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/settings.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/users-functions.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/widgets.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/child-sites.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/dashboard.php';

        $this->loader = new Tbc_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Tbc_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {

        $plugin_i18n = new Tbc_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    private function define_global_hooks()
    {

    }
    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {

        $plugin_admin = new Tbc_Admin($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('admin_enqueue_styles', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
        $this->loader->add_action('admin_footer', $plugin_admin, 'admin_footer');
        $this->loader->add_filter('manage_centre_posts_columns', $plugin_admin, 'set_custom_edit_centre_columns');
        $this->loader->add_action('manage_centre_posts_custom_column', $plugin_admin, 'custom_centre_column', 10, 2);

        //settings page admin
        $settings = new Tbc_Settings($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('admin_menu', $settings, 'add_plugin_page');
        $this->loader->add_action('admin_init', $settings, 'page_init');

        //users custom fields
        $users = new Tbc_Users($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('show_user_profile', $users, 'extra_user_profile_fields');
        $this->loader->add_action('edit_user_profile', $users, 'extra_user_profile_fields');
        $this->loader->add_action('personal_options_update', $users, 'save_extra_user_profile_fields');
        $this->loader->add_action('edit_user_profile_update', $users, 'save_extra_user_profile_fields');
        $this->loader->add_action('init', $users, 'prevent_admin_access');
        $this->loader->add_action('after_setup_theme', $users, 'remove_admin_bar');

        //GForms
        $gforms = new Tbc_GForms($this->get_plugin_name(), $this->get_version());
        $this->loader->add_filter('gppc_replace_merge_tags_in_labels', $gforms, '__return_true');
        //$this->loader->add_filter('gform_notification',$gforms,'PublishEntryNotification',10,3);
        $this->loader->add_filter('gform_notification_events', $gforms, 'gw_add_manual_notification_event');
        //$this->loader->add_action('gform_after_update_entry',$gforms,'gravityview_enable_gf_notifications_after_update',10,3);
        $this->loader->add_filter('gform_merge_tag_data', $gforms, 'my_custom_merge_tag_data', 10, 4);
        //$this->loader->add_action('gravityview/approve_entries/approved',$gforms,'afterEntryApproved',10,2);//removed 22/11/18
        $this->loader->add_action('wp_ajax_nopriv_gv_update_approved', $gforms, 'afterEntryApproved', 10, 2);
        $this->loader->add_action('wp_ajax_gv_update_approved', $gforms, 'afterEntryApproved', 10, 2);

        $this->loader->add_filter('gform_entries_column_filter', $gforms, 'change_column_data', 10, 5);
        $this->loader->add_filter('gform_after_submission_1', $gforms, 'after_submission', 10, 5);

        $toolset = new Tbc_Toolset($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('wp_ajax_getarrs', $toolset, 'getArrs');
        $this->loader->add_action('wp_ajax_nopriv_getarrs', $toolset, 'getArrs');
    }
    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {

        $plugin_public = new Tbc_Public($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');

        //widgets
        $widgets = new Tbc_Widgets($this->get_plugin_name(), $this->get_version());
        $this->loader->add_shortcode('widget_stats_tbc', $widgets, 'getStatsTbcWidget');
        $this->loader->add_shortcode('widget_news', $widgets, 'getNewsWidget');
        $this->loader->add_shortcode('widget_stats_entries', $widgets, 'getStatsEntriesWidget');
        $this->loader->add_shortcode('widget_search_home', $widgets, 'getSearchHomeWidget');
        $this->loader->add_shortcode('widget_banner_results', $widgets, 'getBannerResultsWidget');
        $this->loader->add_shortcode('widget_search_inner', $widgets, 'getSearchInnerWidget');

        //child sites widget
        $child_sites = new Tbc_ChildSites($this->get_plugin_name(), $this->get_version());
        $this->loader->add_shortcode('widget_api_news', $child_sites, 'getNewsWidget');

        //dashboard shortcodes
        $dashboard = new Tbc_Dashboard($this->get_plugin_name(), $this->get_version());
        $this->loader->add_shortcode('dashboard_admin', $dashboard, 'getDashboardAdmin');
        $this->loader->add_action('wp_ajax_getStats', $dashboard, 'getStats');
        $this->loader->add_action('wp_ajax_nopriv_getStats', $dashboard, 'getStats');

        /*

        $this->loader->add_action('wp_ajax_getDashboardTypesStats',$dashboard,'getTypeStats');
        $this->loader->add_action('wp_ajax_nopriv_getDashboardTypesStats',$dashboard,'getTypeStats');
        $this->loader->add_action('wp_ajax_getDashboardStateStats',$dashboard,'getStateStats');
        $this->loader->add_action('wp_ajax_nopriv_getDashboardStateStats',$dashboard,'getStateStats'); */

        //uploader
        $uploader = new Tbc_Uploader($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('wp_ajax_sendproposal', $uploader, 'sendProposal');
        $this->loader->add_action('wp_ajax_nopriv_sendproposal', $uploader, 'sendProposal');
        $this->loader->add_action('wp_ajax_removeproposal', $uploader, 'removeProposal');
        $this->loader->add_action('wp_ajax_nopriv_removeproposal', $uploader, 'removeProposal');
        $this->loader->add_action('wp_ajax_test', $uploader, 'checkOrientation');
        $this->loader->add_action('wp_ajax_uploadFile', $uploader, 'uploadFile');
        $this->loader->add_action('wp_ajax_nopriv_uploadFile', $uploader, 'uploadFile');
        $this->loader->add_action('wp_ajax_uploadProposal', $uploader, 'uploadProposal');
        $this->loader->add_action('wp_ajax_nopriv_uploadProposal', $uploader, 'uploadProposal');
        $this->loader->add_action('wp_ajax_deleteFile', $uploader, 'deleteFile');
        $this->loader->add_action('wp_ajax_nopriv_deleteFile', $uploader, 'deleteFile');
        $this->loader->add_action('wp_enqueue_scripts', $uploader, 'addInlineScript');

        //toolset
        $toolset = new Tbc_Toolset($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('wpv_filter_query', $toolset, 'filterSearchQuery', 10, 3);
        $this->loader->add_filter('wpt_field_options', $toolset, 'populateSelectLocalisation', 10, 5);
        $this->loader->add_action('wp_ajax_getcities', $toolset, 'getCities');
        $this->loader->add_action('wp_ajax_nopriv_getcities', $toolset, 'getCities');
        $this->loader->add_shortcode('frcities_values', $toolset, 'listFrCitiesValues');
        $this->loader->add_shortcode('frcities_display_values', $toolset, 'listFrCitiesDisplayValues');
        $this->loader->add_shortcode('country_values', $toolset, 'listCountriesValues');
        $this->loader->add_shortcode('country_display_values', $toolset, 'listCountriesDisplayValues');
        $this->loader->add_shortcode('frarr_values', $toolset, 'listArrValues');
        $this->loader->add_shortcode('frarr_display_values', $toolset, 'listArrDisplayValues');
        $this->loader->add_filter('wpv_custom_inner_shortcodes', $toolset, 'innerToolsetShortcodes');
        $this->loader->add_shortcode('get_parent_id', $toolset, 'get_parent_id_func');
        $this->loader->add_shortcode('tbc_btn_reservation', $toolset, 'getBtnReservation');
        $this->loader->add_shortcode('tbc_price', $toolset, 'getPriceValue');
        $this->loader->add_shortcode('get_child_id', $toolset, 'getChildId');
        $this->loader->add_shortcode('tbc_slider', $toolset, 'getSearchSlider');
        $this->loader->add_shortcode('wpml_center_id_fr', $toolset, 'getCenterIdFr');
        $this->loader->add_shortcode('tbc_pub', $toolset, 'getPub');
        $this->loader->add_shortcode('tbc_search_product', $toolset, 'getSearchProduct');
        $this->loader->add_shortcode('tbc_breadcrumb', $toolset, 'getBreadcrumb');
        $this->loader->add_action('wp_ajax_getlocation', $toolset, 'getLocation');
        $this->loader->add_action('wp_ajax_nopriv_getlocation', $toolset, 'getLocation');
        $this->loader->add_shortcode('head_address', $toolset, 'getHeadingAddress');
        $this->loader->add_shortcode('term_name', $toolset, 'getTermName');
        $this->loader->add_action('template_redirect', $toolset, 'redirectCenter');
        $this->loader->add_shortcode('count_child_posts', $toolset, 'count_child_posts');

        //Tbc_GForms
        $gforms = new Tbc_GForms($this->get_plugin_name(), $this->get_version());
        $this->loader->add_filter('gform_pre_render', $gforms, 'populate_posts');
        $this->loader->add_filter('gform_pre_validation', $gforms, 'populate_posts');
        $this->loader->add_filter('gform_pre_submission_filter', $gforms, 'populate_posts');
        $this->loader->add_filter('gform_admin_pre_render', $gforms, 'populate_posts');
        $this->loader->add_action('gform_after_submission', $gforms, 'afterSubmission', 10, 2);
        $this->loader->add_action('wp_ajax_updateentrystatus', $gforms, 'updateEntry');
        $this->loader->add_action('wp_ajax_nopriv_updateentrystatus', $gforms, 'updateEntry');
        $this->loader->add_action('wp_ajax_updateentry', $gforms, 'updateEntry');
        $this->loader->add_action('wp_ajax_nopriv_updateentry', $gforms, 'updateEntry');
        $this->loader->add_filter('gform_field_container', $gforms, 'ContainerMarkup', 10, 6);

        //Tbc_GViews
        $gview = new Tbc_GViews($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('gravityview_after', $gview, 'showUploadField');
        $this->loader->add_filter('gravityview_field_entry_value', $gview, 'modifyValuesEspaceMembre', 10, 4);
        $this->loader->add_filter('gform_entry_field_value', $gview, 'customEntryFieldValue', 10, 4);
        $this->loader->add_action('gravityview_search_criteria', $gview, 'my_gv_custom_role_filter');
        $this->loader->add_shortcode('tbc_config', $gview, 'getTbcConfig');
        $this->loader->add_shortcode('tbc_stars', $gview, 'getTbcStars');
        $this->loader->add_shortcode('tbc_applyreadmore', $gview, 'getContentWithReadmore');
        $this->loader->add_action('rest_api_init', $gview, 'recallCronMembers');
        $this->loader->add_action('gravityview/loaded', $gview, 'onInitGravityView');
        $this->loader->add_filter('gravityview/view/entries', $gview, 'gravityview_Entries', 10, 2);
        $this->loader->add_action('gravityview_before', $gview, 'beforeEntry');
        $this->loader->add_action('wp_ajax_updatefieldvalue', $gview, 'updateFieldValue');
        $this->loader->add_action('wp_ajax_nopriv_updatefieldvalue', $gview, 'updateFieldValue');
        $this->loader->add_shortcode('tbc_handle_status_entry', $gview, 'getOutdatedMsg');
        $this->loader->add_shortcode('user_role', $gview, 'userRole');
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Tbc_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version()
    {
        return $this->version;
    }

}
